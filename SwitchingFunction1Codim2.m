classdef SwitchingFunction1Codim2 < SwitchingFunction & DynamicsFunctionIdeal
    %SwitchingFunction1Codim2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Alpha
    end
    
    methods
        function [sf] = SwitchingFunction1Codim2(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k,...
                alpha)
            %SwitchingFunction1Codim2 Construct an instance of this class
            %   Detailed explanation goes here
            sf@SwitchingFunction();
            sf@DynamicsFunctionIdeal(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k);
            sf.Alpha = diag(alpha);
        end
        
        function [u] = vector_field(~, w)
            u = w;
        end
        
        function [v] = potential_function(fds, ~, h)
            % h : the value of the switching function
            v = (1/2)*transpose(h)*fds.Alpha*h;
        end
        
        function [D_t_V] = potential_derivative(fds, t, zx, h, u)
            f = fds.f_der(t, zx, u);
            
            A = fds.Alpha;
            grad_potential = (1/2).*transpose(h)*(A + transpose(A))*fds.J_H;
            
            D_t_V = grad_potential*f;
        end
    end
end

