classdef (Abstract) CircuitProjection < Circuit
    %Circuit Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        zz_1_2_ref
        zz_3_s
    end
    
    methods
        function [circ] = CircuitProjection(dynamicsFunction, switchingFunction, ...
                n_surfaces, ...
                n_main, n_alternative, ...
                zz_1_2_ref, zz_3_s)
            %Circuit Construct an instance of this class
            %   Detailed explanation goes here
            circ@Circuit(dynamicsFunction, switchingFunction, ...
                n_surfaces, ...
                n_main, n_alternative);
            
            circ.zz_1_2_ref = zz_1_2_ref;
            circ.zz_3_s = zz_3_s;
        end
        
        function [f, M] = flow(circ, w)
            %flow Summary of this method goes here
            %   Detailed explanation goes here
            M = eye(3,3);
            u = circ.switchingFunction.vector_field(w);
            fds = circ.dynamicsFunction;
            
            f = @der;
            
            zz_3 = circ.zz_3_s;
            function [D_t_zz] = der(t, zz_1_2_3) % t, zz
                zz_1_2 = zz_1_2_3(1:2);
                zx = fds.physical_space([zz_1_2; zz_3]);
                D_t_zz = fds.T_trs*fds.f_der(t, zx, u);
            end
        end
        
        function [v] = potential_function(circ, t, zz_1_2_3)
            h = zz_1_2_3(1:2);
            v = circ.switchingFunction.potential_function(t, h);
        end
        
        function [D_t_V] = potential_derivative(circ, t, zz_1_2_3, u)
            h = zz_1_2_3(1:2);
            zx = circ.switchingFunction.physical_space([h;circ.zz_3_s]);
            D_t_V = circ.switchingFunction.potential_derivative(t, zx, h, u);
        end
        
        function [x] = nominal_state(circ)
            x = [circ.zz_1_2_ref; 0];
        end
        
        function [circ] = reset_sliding_state(circ, zz_3)
            circ.zz_3_s = zz_3;
        end
        
        % Depreciated function
        function [D_t_zz_3] = averaged_sliding_speed(circ, gamma)
            dfs = circ.dynamicsFunctionsSurf;
            
            zz = [circ.zz_1_2_ref; circ.zz_3_s];
            x = dfs.physical_space(zz);
            
            D_t_x = zeros(size(x));
            for n = 0:1
                for m = 0:1
                    u = [m;n];
                    D_t_x = D_t_x + gamma(m+1,n+1) .* dfs.f_der(x,u);
                end
            end
            
            D_t_z = dfs.T_trs*D_t_x;
            D_t_zz_3 = D_t_z(3);
        end
    end
end

