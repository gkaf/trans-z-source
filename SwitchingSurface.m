classdef (Abstract) SwitchingSurface
    %DynamicsFunctionSurf Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        switchingFunction
    end
    
    methods
        function [df] = SwitchingSurface(switchingFunction)
            %DynamicsFunctionSurf Construct an instance of this class
            %   Detailed explanation goes here
            df.switchingFunction = switchingFunction;
        end
    end
    
    methods (Abstract)
        [flag] = manifold_point_admissible(fds, idx, zx);
        [SZX] = hysteresis_manifolds_projected_space(fds, N, epsilon);
        
        [SZX] = plotable_hysteresis_manifolds_projected_space(fds, N, epsilon);
        [SZX] = segment_plotable_hysteresis_manifolds_projected_space(fds, N, epsilon);
    end
    
    methods
        function [DtS] = potential_derivative(df, zx, u)
            t = 0;
            h = df.switchingFunction.switching_function(t, zx);
            DtS = df.switchingFunction.potential_derivative(t, zx, h, u);
        end
        
        function [v] = potential_function(df, zx)
            t = 0;
            h = df.switchingFunction.switching_function(t, zx); % t, zx
            v = df.switchingFunction.potential_function(t, h);
        end
        
        function [h] = switching_function(df, zx)
            t = 0;
            h = df.switchingFunction.switching_function(t, zx); % t, zx
        end
        
        function [U] = control_inputs(df)
            U = df.switchingFunction.U;
        end
    end
    
    methods
        function [S] = hysteresis_manifolds_projected_physical_1_2(df, N, zx_3_sld, epsilon)
            % Returns a boundary envelloping all the projections on
            % x(1:2) of the hysteresis surfaces for all values of zx_3_sld.
            % The boundary does not have to be convex.
            SZX = df.hysteresis_manifolds_projected_space(N, epsilon);
            
            M_len = length(zx_3_sld);
            shrinking_factor = 0.99; % zero for convex hull, 1 for full fit
            
            S = cell(size(SZX));
            
            for idx = 1:length(SZX)
                S_spec = zeros(3, size(SZX{idx},2));
                S{idx} = zeros(2, 0);
                for m = 1:M_len
                    for n = 1:size(SZX{idx},2)
                        S_spec(:,n) = df.switchingFunction.physical_space([SZX{idx}(:,n); zx_3_sld(m)]);
                    end
                    S_union = [S_spec(1:2,:), S{idx}];
                    x = transpose(S_union(1,:));
                    y = transpose(S_union(2,:));
                    id = boundary(x, y, shrinking_factor);
                    S{idx} = S_union(:,id);
                end
            end
        end
        
        function [S] = hysteresis_manifolds(fds, N, zx_3_sld, epsilon)
            SZX = fds.hysteresis_manifolds_projected_space(N, epsilon);
            
            S = cell(size(SZX));
            for idx = 1:length(SZX)
                M_len = length(zx_3_sld);
                SZX_idx = SZX{idx};
                
                S_idx = zeros(3, size(SZX_idx,2), M_len);
                for m = 1:M_len
                    for n = 1:size(SZX_idx,2)
                        S_idx(:,n,m) = fds.switchingFunction.physical_space([SZX_idx(:,n); zx_3_sld(m)]);
                    end
                end
                S{idx} = S_idx;
            end
        end
        
        function [S] = plotable_hysteresis_manifolds(fds, N, zx_3_sld, epsilon)
            SZX = fds.plotable_hysteresis_manifolds_projected_space(N, epsilon);
            
            M_len = length(zx_3_sld);
            S = zeros(3, size(SZX,2), M_len);
            for m = 1:M_len
                for n = 1:size(SZX,2)
                    S(:,n,m) = fds.switchingFunction.physical_space([SZX(:,n); zx_3_sld(m)]);
                end
            end
        end
        
        function [flag] = manifolds_admissible(df, S)
            all_manifolds_admissible = true;
            
            M = length(S);
            idx = 1;
            while idx <= M && all_manifolds_admissible
                S_idx = S{idx};
                N = size(S_idx, 2);
                manifold_admissible = true;
                n = 1;
                while n <= N && manifold_admissible
                    zx = S_idx(:,n);
                    manifold_admissible = df.manifold_point_admissible(idx, zx);
                    n = n + 1;
                end
                all_manifolds_admissible = manifold_admissible;
                idx = idx + 1;
            end
            
            flag = all_manifolds_admissible;
        end
        
        function [S_flt] = filter_manifold(df, S, idx, u)
            S_idx = S{idx};
            N = size(S_idx,2);
            s_fls = Stack();
            for n = 1:N
                zx = S_idx(:,n);
                DtS_u = df.potential_derivative(zx, u);
                if DtS_u < 0
                    s_fls = s_fls.push(zx);
                end
            end
            
            S_flt = cell2mat(s_fls.toCell());
        end
    end
    
    methods
        function [zz_3_threshold, x_threshold] = stabilityThreshold(df, epsilon_hyst, N, epsilon_convergence_tolerance, zz_1_2_ref)
            zz_3 = 0;
            
            zz_3_step = 1;
            zz_3_low = zz_3;
            S = df.hysteresis_manifolds(N, zz_3_low, epsilon_hyst);
            while df.manifolds_admissible(S)
                zz_3_low = zz_3_low - zz_3_step;
                zz_3_step = 2*zz_3_step;
                S = df.hysteresis_manifolds(N, zz_3_low, epsilon_hyst);
            end
            
            zz_3_step = 1;
            zz_3_high = zz_3;
            S = df.hysteresis_manifolds(N, zz_3_high, epsilon_hyst);
            while ~df.manifolds_admissible(S)
                zz_3_high = zz_3_high + zz_3_step;
                zz_3_step = 2*zz_3_step;
                S = df.hysteresis_manifolds(N, zz_3_high, epsilon_hyst);
            end
            
            zz_3_mid = (zz_3_high + zz_3_low)/2;
            S = df.hysteresis_manifolds(N, zz_3_mid, epsilon_hyst);
            while abs(zz_3_high - zz_3_low) > epsilon_convergence_tolerance
                stable_sliding = df.manifolds_admissible(S);
                if stable_sliding
                    zz_3_high = zz_3_mid;
                else
                    zz_3_low = zz_3_mid;
                end
                zz_3_mid = (zz_3_high + zz_3_low)/2;
                S = df.hysteresis_manifolds(N, zz_3_mid, epsilon_hyst);
            end
            
            zz_3_threshold = zz_3_high;
            
            x_threshold = df.switchingFunction.physical_space([zz_1_2_ref; zz_3_threshold]);
        end
    end
end

