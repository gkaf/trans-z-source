classdef (Abstract) Circuit
    %Circuit Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        dynamicsFunction
        switchingFunction
        
        n_surfaces
        
        n_main
        n_alternative
    end
    
    methods (Abstract)
        [f, M] = flow(circ, w);
        [v] = potential_function(circ, t, zx);
        [D_t_V] = potential_derivative(circ, t, zx, u);
        [z_ref] = nominal_state(circ);
    end
    
    methods (Abstract)
        % idx_S - to consider only specific index of S
        [switchingRule_cell] = critical_function(circ, idx_S, t_fin, zx_fin, u);
        [zz] = alternative_space(circ, zx);
    end
    
    methods
        function [circ] = Circuit(dynamicsFunction, switchingFunction, ...
                n_surfaces, ...
                n_main, n_alternative)
            %Circuit Construct an instance of this class
            %   Detailed explanation goes here
            circ.dynamicsFunction = dynamicsFunction;
            circ.switchingFunction = switchingFunction;
            
            circ.n_surfaces = n_surfaces;
            
            circ.n_main = n_main;
            circ.n_alternative = n_alternative;
        end
    end
end

