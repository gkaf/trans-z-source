classdef DynamicFlowIntegrator < FlowIntegrator
    %FilterFlowIntegrator Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/FilterFlowIntegrator.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties (Constant)
        solver = @ode45;
    end
    
    methods
        function [ffi] = DynamicFlowIntegrator(rel_tol, abs_tol, max_step)
            ffi@FlowIntegrator(rel_tol, abs_tol, max_step);
        end
    end
    
end

