classdef TrajectoryAnalysis
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/TrajectoryAnalysis.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        eras
        discreteStateGenerator
    end
    
    methods
        function [ta] = TrajectoryAnalysis(transcript)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            ta.eras = transcript.getEras();
            ta.discreteStateGenerator = transcript.discreteStateGenerator;
        end
        
        function [cycle_eras] = findCycle(ta, n_0, epsilon)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            n = n_0;
            N = length(ta.eras);
            while isa(ta.eras{n}, 'StorableJumpEraTranscript') && n < N
                n = n + 1;
            end
            
            x_0 = ta.eras{n}.x_init;
            m = n;
            cycle_found = false;
            while m < N && not(cycle_found)
                m = m+1;
                while isa(ta.eras{m}, 'StorableJumpEraTranscript') && m < N
                    m = m+1;
                end
                
                if m <= N
                    x_m = ta.eras{m}.x_init;
                    d = norm(x_0 - x_m, 2);
                    cycle_found = d < epsilon;
                end
            end
            
            if cycle_found
                dk = 1;
                cycle_eras = cell(1, m-n);
                for k = n:(m-1)
                    cycle_eras{dk} = ta.eras{k};
                    dk = dk + 1;
                end
            else
                cycle_eras = {};
            end
        end
        
        function [cycle_transcrpt] = getCycle(ta, n_0, epsilon)
            cycle_eras = ta.findCycle(n_0, epsilon);
            cycle_transcrpt = ta.makeTranscript(cycle_eras);
        end
        
        function [transcript] = getTranscrpt(ta, n, m)
            eras_traj = cell(1, m-n+1);
            d = 1;
            for k = n:m
                eras_traj{d} = ta.eras{k};
                d = d + 1;
            end
            transcript = ta.makeTranscript(eras_traj);
        end
        
        function [transcipt] = makeTranscript(ta, eras)
            transcipt = Transcript(ta.discreteStateGenerator);
            
            for n = 1:length(eras)
                transcipt = transcipt.extend(eras{n});
            end
        end
    end
end

