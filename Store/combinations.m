function [U] = combinations(k,n)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if k == 0
    U = zeros(n,1);
elseif k == n
    U = ones(n,1);
else
    sf_0 = combinations(k, n-1);
    sf_1 = combinations(k-1, n-1);
    
    M = size(sf_0, 2) + size(sf_1, 2);
    U = zeros(n, M);
    
    m_U = 1;
    m = 1;
    while m <= size(sf_0, 2)
        U(:,m_U) = [0; sf_0(:,m)];
        m = m + 1;
        m_U = m_U + 1;
    end
    m = 1;
    while m <= size(sf_1, 2)
        U(:,m_U) = [1; sf_1(:,m)];
        m = m + 1;
        m_U = m_U + 1;
    end
end

end

