classdef InitializeCycleDetection < MapFunctions & DiscreteStateJump
    %InitializeCycleDetection Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        cycleDetection
    end
    
    methods
        function [ds] = InitializeCycleDetection(u_output, dynamicsFunctions, continuum_output_size)
            %InitializeCycleDetection Construct an instance of this class
            %   Detailed explanation goes here
            
            ds@MapFunctions(dynamicsFunctions);
            ds@DiscreteStateJump(TransZSourceDiscreteStateEnumeration.Initial, u, continuum_output_size);
            
            ds.discreteStateData = CycleDetection(u_output);
        end
        
        function [ind] = isTerminal(~, ~)
            %isTerminal Summary of this method goes here
            %   Detailed explanation goes here
            ind = false;
        end
        
        function [e_idx] = guard(~, ~, ~) % ds, t, x
            e_idx = 1;
        end
        
        function [stateId, x_post] = jump(ds, ~, t, x_pre) % ds, e_idx, t, x_pre
            discreteStateEnumeration = TransZSourceDiscreteStateEnumeration.Loop;
            
            u = ds.u;
            stateId = StateId(discreteStateEnumeration, u);
            
            x_post = [x_pre; t];
        end
        
        function [x] = hologram(~, ~, zx)
            x = zx;
        end
    end
end

