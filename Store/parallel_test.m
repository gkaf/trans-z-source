[~,default] = parallel.clusterProfiles();
clst = parcluster(default);
clst.NumWorkers = 4;
pool = parpool(clst,4);

N = 16;

f = cell(N,1);
for idx = 1:N
    f{idx} = parallel.FevalFuture();
end
f = cat(1,f{:});

tic

for idx = 1:N
    f(idx) = parfeval(pool,@pause,0,10); % Square size determined by idx
end

magicResults = cell(N,1);
for idx = 1:N
    % fetchNext blocks until next results are available.
    %[completedIdx,value] = fetchNext(f);
    [completedIdx] = fetchNext(f);
    %magicResults{completedIdx} = value;
    fprintf('Got result with index: %d.\n', completedIdx);
end

Delta_t = toc


function [slidingAnalysisResults] = slidingSpeedContinuation(ssp, zz_3_s, ZZ_3_s_series_lt, ZZ_3_s_series_bt)
            %slidingSpeed Summary of this method goes here
            %   Detailed explanation goes here
            discreteStateGenerator = ssp.getDiscreteStateGenerator();
            M = 2;
            
            [~, default_clst] = parallel.clusterProfiles();
            clst = parcluster(default_clst);
            clst.NumWorkers = M;
            pool = parpool(clst,M);
            
            x_0 = ssp.circuitProjection.nominal_state();
            u_0 = [1;1];
            
                tic;
            result_0 = ssp.slidingSpeed(discreteStateGenerator, zz_3_s, x_0, u_0);
            Dt = toc;
            
            if isa(result_0, 'MeanStates')
                meanState = result_0;
                [x_0, u_0] = meanState.finalStates();
                slidingAnalysisResult_0 = WellDefinedSlidingSpeed(meanState, zz_3_s, Dt);
            else
                slidingAnalysisResult_0 = NotWellDefinedSlidingSpeed(result_0, zz_3_s, Dt);
            end
            
            f = cell(2,1);
            for idx = 1:length(f)
                f{idx} = parallel.FevalFuture();
            end
            f = cat(1,f{:});
            
            idx_bt = 1;
            idx_lt = 2;
            
            N = cell(2,1);
            N{idx_bt} = length(ZZ_3_s_series_bt);
            N{idx_lt} = length(ZZ_3_s_series_lt);
            slidingAnalysisResults_bt = cell(length(ZZ_3_s_series_bt), 1);
            slidingAnalysisResults_lt = cell(length(ZZ_3_s_series_lt), 1);
            
            zz_1_2_0 = cell(2,1);
            zz_1_2_0{idx_bt} = x_0;
            zz_1_2_0{idx_lt} = x_0;
            u_0_s{idx_bt} = u_0;
            u_0_s{idx_lt} = u_0;
            
            if 1 <= N{idx_bt}
                f(idx_bt) = parfeval(pool, get_problem(discreteStateGenerator, ZZ_3_s_series_bt(1), zz_1_2_0{idx_bt}, u_0_s{idx_bt}), 1);
            end
            if 1 <= N{idx_lt}
                f(idx_lt) = parfeval(pool, get_problem(discreteStateGenerator, ZZ_3_s_series_lt(1), zz_1_2_0{idx_lt}, u_0_s{idx_lt}), 1);
            end
            
            n_recieved = cell(2,1);
            n_recieved{idx_bt} = 0;
            n_recieved{idx_lt} = 0;
            n_submiting = cell(2,1);
            n_submiting{idx_bt} = 2;
            n_submiting{idx_lt} = 2;
            while n_recieved{idx_bt} < N{idx_bt} || n_recieved{idx_lt} < N{idx_lt}
                [idx, result] = fetchNext(f);
                Dt = seconds(f(idx).FinishDateTime - f(idx).StartDateTime);
                if idx == idx_bt
                    n_recieved{idx_bt} = n_recieved{idx_bt} + 1;
                    n = n_recieved{idx_bt};
                    zz_3_s = ZZ_3_s_series_bt(n);
                    if isa(result, 'MeanStates')
                        meanState = result;
                        [zz_1_2_0{idx_bt}, u_0_s{idx_bt}] = meanState.finalStates();
                        slidingAnalysisResults_bt{n} = WellDefinedSlidingSpeed(meanState, zz_3_s, Dt);
                    else
                        slidingAnalysisResults_bt{n} = NotWellDefinedSlidingSpeed(result_0, zz_3_s, Dt);
                    end
                    if n_submiting{idx_bt} <= N{idx_bt}
                        f(idx_bt) = parfeval(pool, get_problem(discreteStateGenerator, ZZ_3_s_series_bt(1), zz_1_2_0{idx_bt}, u_0), 1);
                        n_submiting{idx_bt} = n_submiting{idx_bt} + 1;
                    end
                else
                    n_recieved{idx_lt} = n_recieved{idx_lt} + 1;
                    n = n_recieved{idx_lt};
                    zz_3_s = ZZ_3_s_series_lt(n);
                    if isa(result, 'MeanStates')
                        meanState = result;
                        [zz_1_2_0{idx_lt}, u_0_s{idx_lt}] = meanState.finalStates();
                        slidingAnalysisResults_lt{n} = WellDefinedSlidingSpeed(meanState, zz_3_s, Dt);
                    else
                        slidingAnalysisResults_lt{n} = NotWellDefinedSlidingSpeed(result_0, zz_3_s, Dt);
                    end
                    if n_submiting{idx_lt} <= N{idx_lt}
                        f(idx_lt) = parfeval(pool, get_problem(discreteStateGenerator, ZZ_3_s_series_lt(1), zz_1_2_0{idx_lt}, u_0), 1);
                        n_submiting{idx_lt} = n_submiting{idx_lt} + 1;
                    end
                end
            end
            
            slidingAnalysisResults = {slidingAnalysisResults_lt{:}; slidingAnalysisResult_0, slidingAnalysisResults_bt{:}};
            
            delete(pool);
            
            function [f] = get_problem(discreteStateGenerator, zz_3_s, zz_1_2_init, u_0)
                f = @evaluate;
                function [result] = evaluate()
                    result = ssp.slidingSpeed(discreteStateGenerator, zz_3_s, zz_1_2_init, u_0);
                end
            end
        end