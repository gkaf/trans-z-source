% Simple script to polot the cycle

% z-source/CyclePlots.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

trajectory_analysis = TrajectoryAnalysis(transcript);
figure; hold on
cycle_transcrpt = trajectory_analysis.getCycle(3, 1);
[ts, ys, us] = cycle_transcrpt.get_tyu();
plot(ys(4,:), ys(5,:))
cycle_transcrpt = trajectory_analysis.getCycle(2000, 0.5);
[ts, ys, us] = cycle_transcrpt.get_tyu();
plot(ys(4,:), ys(5,:))
cycle_transcrpt = trajectory_analysis.getCycle(14000, 0.5);
[ts, ys, us] = cycle_transcrpt.get_tyu();
plot(ys(4,:), ys(5,:))
hold off