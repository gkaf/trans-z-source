function [slidingAnalysis] = getSlidingAnalysisZSourceProjFlat()
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
[dynamics_functions, epsilon] = getDynamicsFunction1Codim2SurfFlat();
circuitProj = ZSourceProj(dynamics_functions);

quadrant_number_of_points = 100;
Dt_cross = 0.0002;
Dt_period = 0.032;
e_tol = 1e-12;
max_step = 1e-5;

convergence_tolerance = 0.5e-3;

Dt_burn_in = 0.032;
N_ub = 256;
reduction_ratio = 0.5;

slidingAnalysis = SlidingAnalysis(circuitProj, ...
    quadrant_number_of_points, epsilon, ...
    Dt_burn_in, reduction_ratio, Dt_cross, Dt_period, N_ub, ...
    e_tol, max_step, convergence_tolerance);
end

