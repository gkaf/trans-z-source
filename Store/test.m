function [t,x,te,xe,ie] = test()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

options = odeset('RelTol', 1e-6, 'AbsTol', 1e-6, 'MaxStep', 1e-2);
options = odeset(options, 'Events', @e);

[t,x,te,xe,ie] = ode45(@(t,x)(2*x), [0,10^(-6)], 0.1, options);

function [pos, ter_ind, dir] = e(~,x) % t,x
    pos = [x - 0.5; x-1; 2*x-2];
    ter_ind = [0;1;1];
    dir = [1;1;1];
end

end

