classdef DynamicsGenerator
    %DynamicsGenerator Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess=immutable)
        L
        n
        L_mag
        C
        
        E_source
        E_load
        
        r
        r_ESR
        
        i_ref
        k
        alpha
        epsilon
        
        tolerance
        
        % Memoized parameters
        dynamics_functions
    end
    
    properties
        v_ref
    end
    
    methods
        function [dg] = DynamicsGenerator(L, n, L_mag, C, ...
                E_source, E_load, ...        
                r, r_ESR, ...
                v_ref, P_in, k, alpha, epsilon, ...
                tolerance)
            %DynamicsGenerator Construct an instance of this class
            %   Detailed explanation goes here
            dg.L = L;
            dg.n = n;
            dg.L_mag = L_mag;
            dg.C = C;
            dg.E_source = E_source;
            dg.E_load = E_load;
            
            % i_ref
            % v_ref = 480;
            dg.r = r;
            dg.r_ESR = r_ESR;
            dg.k = k;
            dg.alpha = alpha;
            
            dg.tolerance = tolerance;
            
            dg.v_ref = v_ref;
            dg.i_ref = P_in/E_load;
            dg.epsilon = epsilon;
            
            % Memoized
            dg.dynamics_functions =  DynamicsFunction(dg.L, dg.n, dg.L_mag, dg.C, ...
                dg.E_source, dg.E_load, ...
                dg.r, dg.r_ESR, ...
                dg.i_ref, dg.v_ref, dg.k, dg.alpha, ...
                dg.tolerance);
        end
        
        function [dynamics_functions] = get_dynamics(dg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            dynamics_functions =  DynamicsFunction(dg.L, dg.n, dg.L_mag, dg.C, ...
                dg.E_source, dg.E_load, ...
                dg.r, dg.r_ESR, ...
                dg.i_ref, dg.v_ref, dg.k, dg.alpha, ...
                dg.tolerance);
        end
        
        function [dynamic_maps_functions] = get_dynamic_maps(dg, zz_3_s)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            dynamic_maps_functions = DynamicMapsFunctions(dg.L, dg.n, dg.L_mag, dg.C, ...
                dg.E_source, dg.E_load, ...
                dg.r, dg.r_ESR, ...
                dg.i_ref, dg.v_ref, dg.k, dg.alpha, ...
                dg.tolerance, ...
                zz_3_s);
        end
        
        function [stateGenerator] = getCycleDetectionDiscreteStateGenerator(dg, ...
                rel_tol, abs_tol, max_step, ...
                Dt_cross, Dt_cycle, ...
                zz_3_s)
            
            dynamic_maps_functions = dg.get_dynamic_maps(zz_3_s);
            stateGenerator = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
                dynamic_maps_functions, ...
                dg.epsilon, ...
                Dt_cross, Dt_cycle);
        end
    end
    
    methods
        function [SZX] = projected_eliptical_hysteresis_surface(dg, N)
            SZX = dg.dynamics_functions.projected_eliptical_hysteresis_surface(N, dg.epsilon);
        end
        
        function [ZS_flt_00, ZS_flt_10, ZS_flt_01, ZS_flt_11] = mode_admissible_regions(dg, ZS, zz_3_s)
            dynamic_maps_functions = dg.dynamics_functions.getDynamicMapsFunctions(zz_3_s);
            
            ZS_flt_00 = dynamic_maps_functions.filter_projected_points(ZS, [0;0]);
            ZS_flt_10 = dynamic_maps_functions.filter_projected_points(ZS, [1;0]);
            ZS_flt_01 = dynamic_maps_functions.filter_projected_points(ZS, [0;1]);
            ZS_flt_11 = dynamic_maps_functions.filter_projected_points(ZS, [1;1]);
        end
        
        function [ZS_adm] = union_admissible_regions(dg, ZS, zz_3_s)
            [ZS_flt_00, ZS_flt_10, ZS_flt_01, ZS_flt_11] = dg.mode_admissible_regions(ZS, zz_3_s);
            
            ZS_adm = dg.vecsetunion(ZS_flt_00, ZS_flt_10);
            ZS_adm = dg.vecsetunion(ZS_adm, ZS_flt_01);
            ZS_adm = dg.vecsetunion(ZS_adm, ZS_flt_11);
        end
        
        function [ZZ_3_range] = project_into_normal_3(dg, V_C_range)
            ZZ_3_range = zeros(size(V_C_range));
            N_V_C_range = length(V_C_range);
            for idx = 1:N_V_C_range
                v_C = V_C_range(idx);
                ZZ_3_range(idx) = dg.dynamics_functions.project_into_normal_3(v_C);
            end
        end
        
        function [V_C_range] = project_into_physical_3(dg, ZZ_3_range)
            V_C_range = zeros(size(ZZ_3_range));
            N_V_C_range = length(V_C_range);
            for idx = 1:N_V_C_range
                zz_3 = ZZ_3_range(idx);
                zx = dg.dynamics_functions.physical_space([0;0;zz_3]);
                V_C_range(idx) = zx(3);
            end
        end
        
        function [dg] = setVotageReference(dg, v_ref)
            dg.v_ref = v_ref;
        end
    end
    
    methods    
        function [flag] = vecismember(fds, v, S)
            N = size(S, 2);
            idx = 1;
            flag = false;
            while ~flag && idx <= N
                dx = v - S(:,idx);
                flag = norm(dx,2) < fds.tolerance;
                idx = idx + 1;
            end
        end
        
        function [flag] = vecsetincl(fds, S_1, S_2)
            N = size(S_1, 2);
            idx = 1;
            flag = true;
            while flag && idx <= N
                v = S_1(:,idx);
                flag = fds.vecismember(v, S_2);
                idx = idx + 1;
            end
        end
        
        function [S_insc] = vecsetintersection(fds, S_1, S_2)
            N = size(S_1, 2);
            s = Stack();
            for idx = 1:N
                v = S_1(:,idx);
                if fds.vecismember(v, S_2)
                    s = s.push(v);
                end
            end
            S_insc = cell2mat(s.toCell());
        end
        
        function [S_union] = vecsetunion(fds, S_1, S_2)
            N = size(S_1, 2);
            s = Stack();
            for idx = 1:N
                v = S_1(:,idx);
                s = s.push(v);
            end
            
            N = size(S_2, 2);
            for idx = 1:N
                v = S_2(:,idx);
                if ~fds.vecismember(v, S_1)
                    s = s.push(v);
                end
            end
            
            S_union = cell2mat(s.toCell());
        end
    end
end

