classdef TimePlots
    %TimePlots Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess=immutable)
        e_tol
        max_step
        
        Dt_fin
        Dt_state_ub
        Dt_ub
        
        t_0
        u_0
        
        dynamicsGenerator
        abs_tol
        rel_tol
        
        x_0
    end
    
    methods
        function [tp] = TimePlots(Dt_fin, Dt_state_ub, Dt_ub, ...
                e_tol, max_step, ...
                t_0, u_0)
            %TimePlots Construct an instance of this class
            %   Detailed explanation goes here
            tp.e_tol = e_tol;
            tp.max_step = max_step;
            
            tp.Dt_fin = Dt_fin;
            tp.Dt_state_ub = Dt_state_ub;
            tp.Dt_ub = Dt_ub;
            
            tp.t_0 = t_0;
            tp.u_0 = u_0;
            
            tp.dynamicsGenerator = DynamicsGenerator();
            
            n_zx = TimePlots.dynamics_dimensions(tp.dynamicsGenerator);
            tp.abs_tol = e_tol .* ones(n_zx);
            tp.rel_tol = e_tol;
            
            tp.x_0 = TimePlots.initial_condition(tp.dynamicsGenerator);
        end
        
        function save_simulation_time_plots(tp, transient_filename, stable_filename)
            [ts, ys, us] = tp.simulation_time_plots();
            [m, D_st] = TimePlots.duty_ratios(ts, us);
            
            data = [transpose(ts), transpose(ys), transpose(us)];
            dlmwrite(transient_filename, data, 'delimiter', '\t');
            
            names = [var2str(m); var2str(D_st)];
            values = [m; D_st];
            fid = fopen(stable_filename, 'w');
            fprintf(fid,'%s\t%f\n', names(1), values(1));
            fprintf(fid,'%s\t%f\n', names(2), values(2));
            fclose(fid);
        end
        
        function [ts, ys, us] = simulation_time_plots(tp)
            %simulation_time_plots Summary of this method goes here
            %   Detailed explanation goes here
            epsilon = tp.hysteresis();
            dynamicsFunctions = tp.dynamicsGenerator.get_dynamics();
            state_gen = TransZSourceDiscreteStateGenerator(tp.rel_tol, tp.abs_tol, tp.max_step, ...
                dynamicsFunctions, ...
                epsilon, ...
                tp.Dt_fin);
            
            ha = HybridAutomaton(state_gen);
            
            discreteStateData = Transcript(state_gen);
            initial_stateId = StateId(TransZSourceDiscreteStateEnumeration.Initial, discreteStateData, tp.u_0);
            hae = HybridAutomatonEvaluation(ha, tp.Dt_state_ub, tp.Dt_ub, initial_stateId, tp.t_0, tp.x_0);
            
            transcript = hae.evaluate();
            [ts, ys, us] = transcript.get_tyu();
        end
        
        function [epsilon] = hysteresis(tp)
            epsilon = tp.dynamicsGenerator.epsilon;
        end
    end
    
    methods (Static)
        function [m, D_st] = duty_ratios(ts, us)
            N = floor(length(ts)*(5/6));
            D_ST = us(1,N:end).*us(2,N:end);
            D_st = mean(D_ST);
            M = us(1,N:end) - us(2,N:end);
            m = mean(M);
        end
        
        function [n_zx] = dynamics_dimensions(dynamicsGenerator)
            dynamicsFunctions = dynamicsGenerator.get_dynamics();
            n_zx = size(dynamicsFunctions.x_ref);
        end
        
        function [zx_0] = initial_condition(dynamicsGenerator)
            dynamicsFunctions = dynamicsGenerator.get_dynamics();
            zx_0 = dynamicsFunctions.x_ref;
        end
    end
end

