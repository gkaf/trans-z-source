classdef SlidingAnalysis
    %SlidingAnalysis Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        dynamicsGenerator
        quadrant_number_of_points
        
        Dt_cross
        Dt_cycle
        e_tol
        max_step
        
        % Cycle detection parameters
        max_depth
        eta_tolerance
        
        N_burnin_init
        N_burnin_semistable
        
        Dt_state_ub
        Dt_ub
        
        % Serch time and control input initialization
        t_0
        u_0
    end
    
    methods
        function [sa] = SlidingAnalysis(dynamicsGenerator, N, ...
                Dt_cross, Dt_cycle, e_tol, max_step, ...
                max_depth, eta_tolerance, ...
                N_burnin_init, N_burnin_semistable, ...
                Dt_state_ub, Dt_ub, ...
                t_0, u_0)
            %SlidingAnalysis Construct an instance of this class
            %   Detailed explanation goes here
            sa.dynamicsGenerator = dynamicsGenerator;
            sa.quadrant_number_of_points = N;
            
            sa.Dt_cross = Dt_cross;
            sa.Dt_cycle = Dt_cycle;
            sa.e_tol = e_tol;
            sa.max_step =  max_step;
            
            % Cycle detection parameters
            sa.max_depth = max_depth;
            sa.eta_tolerance = eta_tolerance;
            
            sa.N_burnin_init = N_burnin_init;
            sa.N_burnin_semistable = N_burnin_semistable;
            
            sa.Dt_state_ub = Dt_state_ub;
            sa.Dt_ub = Dt_ub;
            
            sa.t_0 = t_0;
            sa.u_0 = u_0;
        end
        
        function [stable_ZZ_3_range, flag] = stablePoints(sa, ZZ_3_range)
            %stablePoints Filters a set of points in the sliding vector
            %space for stable projected dynamics
            %   Given a set of points in the sliding vector space, the
            %   function determines the set of points whose dyanamics
            %   projected in the range space of the objectove function
            %   whose null space is the sliding manifold result in a
            %   hysteresis zone that is time invariant.
            dynamics_generator = sa.dynamicsGenerator;
            N = sa.quadrant_number_of_points;
            
            ZS = dynamics_generator.projected_eliptical_hysteresis_surface(N);
            
            N_ZZ_3 = length(ZZ_3_range);
            
            flag = false(size(ZZ_3_range));
            for n = 1:N_ZZ_3
                zz_3_s = ZZ_3_range(n);
                ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_s);
                
                %stable_sliding = dynamics_generator.vecsetincl(ZS, ZS_adm);
                stable_sliding = size(ZS,2) == size(ZS_adm,2);
                
                flag(n) = stable_sliding;
            end
            
            stable_ZZ_3_range = ZZ_3_range(flag);
        end
        
        function [stable_V_C_range] = stablePhysicalPoints(sa, V_C_range)
            ZZ_3_range = dynamics_generator.project_into_normal_3(V_C_range);
            [~, flag] = sa.stablePoints(ZZ_3_range);
            stable_V_C_range = V_C_range(flag);
        end
        
        function [zz_3_threshold, V_C_threshold] = stabilityThreshold(sa)
            dynamics_generator = sa.dynamicsGenerator;
            N = sa.quadrant_number_of_points;
            
            v_ref = dynamics_generator.v_ref;
            v_low = 0;
            v_high = 2*v_ref;
            
            zz_3 = dynamics_generator.project_into_normal_3(v_ref);
            zz_3_low = dynamics_generator.project_into_normal_3(v_low);
            zz_3_high = dynamics_generator.project_into_normal_3(v_high);
            
            ZS = dynamics_generator.projected_eliptical_hysteresis_surface(N);
            
            ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_low);
            stable_sliding = size(ZS,2) == size(ZS_adm,2);
            while stable_sliding
                zz_3_low = zz_3_low - zz_3;
                ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_low);
                stable_sliding = size(ZS,2) == size(ZS_adm,2);
            end
            
            ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_high);
            stable_sliding = size(ZS,2) == size(ZS_adm,2);
            while ~stable_sliding
                zz_3_high = zz_3_high + zz_3;
                ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_high);
                stable_sliding = size(ZS,2) == size(ZS_adm,2);
            end
            
            zz_3_mid = (zz_3_high + zz_3_low)/2;
            ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_mid);
            stable_sliding = size(ZS,2) == size(ZS_adm,2);
            while abs(zz_3_high - zz_3_low) > sa.eta_tolerance
                if stable_sliding
                    zz_3_high = zz_3_mid;
                else
                    zz_3_low = zz_3_mid;
                end
                zz_3_mid = (zz_3_high + zz_3_low)/2;
                ZS_adm = dynamics_generator.union_admissible_regions(ZS, zz_3_mid);
                stable_sliding = size(ZS,2) == size(ZS_adm,2);
            end
            
            zz_3_threshold = zz_3_high;
            V_C_threshold = dynamics_generator.project_into_physical_3(zz_3_threshold);
        end
        
        function [cycleAnalysisResult] = slidingTrajectory(sa, zz_3_s)
            dynamic_maps_functions = sa.dynamicsGenerator.get_dynamic_maps(zz_3_s);
            
            rel_tol = sa.e_tol;
            abs_tol = sa.e_tol .* ones(size(dynamic_maps_functions.zz_1_2_ref));
            
            stateGenerator = sa.dynamicsGenerator.getCycleDetectionDiscreteStateGenerator(...
                rel_tol, abs_tol, sa.max_step, ...
                sa.Dt_cross, sa.Dt_cycle, ...
                zz_3_s);
            
            trajectoryCheckPoints = TrajectoryCheckPoints(sa.max_depth, sa.eta_tolerance);
            burnInState = BurnInState(trajectoryCheckPoints, sa.N_burnin_init);
            
            ha = HybridAutomaton(stateGenerator);
            
            discreteStateData = burnInState;
            x_0 = dynamic_maps_functions.zz_1_2_ref;
            initial_stateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, discreteStateData, sa.u_0);
            hae = HybridAutomatonEvaluation(ha, sa.Dt_state_ub, sa.Dt_ub, initial_stateId, sa.t_0, x_0);
            
            cycleAnalysisResult = hae.evaluate();
        end
        
        function [slidingAnalysisResults] = slidingSpeed(sa, ZZ_3_s)
            N = length(ZZ_3_s);
            results = cell(N,1);
            
            slidingAnalysisResult = {};
            for n = 1:N
                zz_3_s = ZZ_3_s(n);
                
                fprintf('Analysing sample %d with value: z=%.1f\n', n, zz_3_s)
                tic;
                
                dynamic_maps_functions = sa.dynamicsGenerator.get_dynamic_maps(zz_3_s);
                
                rel_tol = sa.e_tol;
                abs_tol = sa.e_tol .* ones(size(dynamic_maps_functions.zz_1_2_ref));
                
                stateGenerator = sa.dynamicsGenerator.getCycleDetectionDiscreteStateGenerator(...
                    rel_tol, abs_tol, sa.max_step, ...
                    sa.Dt_cross, sa.Dt_cycle, ...
                    zz_3_s);
                
                trajectoryCheckPoints = TrajectoryCheckPoints(sa.max_depth, sa.eta_tolerance);
                if isa(slidingAnalysisResult, 'StableCycleSlidingAnalysisResult')
                    burnInState = BurnInState(trajectoryCheckPoints, sa.N_burnin_semistable);
                    stableCycleSlidingAnalysisResult = slidingAnalysisResult;
                    x_0 = stableCycleSlidingAnalysisResult.finalCyclePoint();
                else
                    burnInState = BurnInState(trajectoryCheckPoints, sa.N_burnin_init);
                    x_0 = dynamic_maps_functions.zz_1_2_ref;
                end
                
                ha = HybridAutomaton(stateGenerator);
                
                discreteStateData = burnInState;
                
                initial_stateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, discreteStateData, sa.u_0);
                hae = HybridAutomatonEvaluation(ha, sa.Dt_state_ub, sa.Dt_ub, initial_stateId, sa.t_0, x_0);
                
                cycleAnalysisResult = hae.evaluate();
                
                if isa(cycleAnalysisResult, 'Cycle')
                    finiteTranscriptEvaluation = cycleAnalysisResult.finiteTranscriptEvaluation;
                    slidingAnalysisResult = StableCycleSlidingAnalysisResult(finiteTranscriptEvaluation, zz_3_s, dynamic_maps_functions);
                elseif isa(cycleAnalysisResult, 'Chaotic')
                    finiteTranscriptEvaluation = cycleAnalysisResult.finiteTranscriptEvaluation;
                    slidingAnalysisResult = ChaoticSlidingAnalysisResult(finiteTranscriptEvaluation, zz_3_s, dynamic_maps_functions);
                elseif isa(cycleAnalysisResult, 'CycleEvaluationTimedOut')
                    slidingAnalysisResult = UnstableSlidingAnalysisResult(zz_3_s);
                else
                    slidingAnalysisResult = UnstableSlidingAnalysisResult(zz_3_s);
                end
                results{n} = slidingAnalysisResult;
                
                Delta_t = toc;
                fprintf('Time spent in analysis: %f\n', Delta_t);
            end
            
            slidingAnalysisResults = SlidingAnalysisResults(results);
        end
    end
    
    methods
        function [ZS_flt_00, ZS_flt_10, ZS_flt_01, ZS_flt_11] = mode_admissible_regions(sa, ZS, v_ref, zz_3_s)
            dynamics_generator = sa.dynamicsGenerator;
            dynamics_generator = dynamics_generator.setVotageReference(v_ref);
            dynamic_maps_functions = dynamics_generator.get_dynamic_maps(zz_3_s);
            
            ZS_flt_00 = dynamic_maps_functions.filter_projected_points(ZS, [0;0]);
            ZS_flt_10 = dynamic_maps_functions.filter_projected_points(ZS, [1;0]);
            ZS_flt_01 = dynamic_maps_functions.filter_projected_points(ZS, [0;1]);
            ZS_flt_11 = dynamic_maps_functions.filter_projected_points(ZS, [1;1]);
        end
        
        function [ZS_adm] = union_admissible_regions(sa, ZS, v_ref, zz_3_s)
            dynamics_generator = sa.dynamicsGenerator;
            
            [ZS_flt_00, ZS_flt_10, ZS_flt_01, ZS_flt_11] = sa.mode_admissible_regions(ZS, v_ref, zz_3_s);
            
            ZS_adm = dynamics_generator.vecsetunion(ZS_flt_00, ZS_flt_10);
            ZS_adm = dynamics_generator.vecsetunion(ZS_adm, ZS_flt_01);
            ZS_adm = dynamics_generator.vecsetunion(ZS_adm, ZS_flt_11);
        end
        
        function [flt_V_ref] = stableConfigurations(sa, V_ref, ZZ_3)
            dynamics_generator = sa.dynamicsGenerator;
            N = sa.quadrant_number_of_points;
            
            ZS = dynamics_generator.projected_eliptical_hysteresis_surface(N);
            
            N_ZZ = length(ZZ_3);
            N_V_ref = length(V_ref);
            
            flag = false(size(V_ref));
            for n = 1:N_V_ref
                v_ref = V_ref(n);
                
                stable_sliding = true;
                k = 1;
                while stable_sliding && k <= N_ZZ
                    zz_3_s = ZZ_3(k);
                    ZS_adm = sa.union_admissible_regions(ZS, v_ref, zz_3_s);
                    stable_sliding = dynamics_generator.vecsetincl(ZS, ZS_adm);
                    k = k + 1;
                end
                flag(n) = stable_sliding;
            end
            
            flt_V_ref = V_ref(flag);
        end
    end
end

