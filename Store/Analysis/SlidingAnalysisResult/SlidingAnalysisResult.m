classdef (Abstract) SlidingAnalysisResult
    %SlidingAnalysisResult Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        zz
    end
    
    methods (Abstract)
        [flag] = isStableSliding(sar);
        [flag] = isStableCycle(sar);
    end
    
    methods
        function [sar] = SlidingAnalysisResult(zz)
            %SlidingAnalysisResult Construct an instance of this class
            %   Detailed explanation goes here
            
            sar.zz = zz;
        end
        
        function [zz] = zzLocation(sar)
            %zzLocation Summary of this method goes here
            %   Detailed explanation goes here
            zz = sar.zz;
        end
    end
end

