classdef UnstableSlidingAnalysisResult < SlidingAnalysisResult
    %ChaoticSlidingAnalysisResult Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [sar] = UnstableSlidingAnalysisResult(zz)
            %UnstableSlidingAnalysisResult Construct an instance of this class
            %   Detailed explanation goes here
            sar@SlidingAnalysisResult(zz)
        end
        
        function [flag] = isStableCycle(~)
            flag = false;
        end
        
        function [flag] = isStableSliding(~)
            flag = false;
        end
    end
end

