classdef SlidingAnalysisResults
    %SlidingAnalysisResults Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        slidingAnalysisResults
    end
    
    methods
        function [sar] = SlidingAnalysisResults(slidingAnalysisResults)
            %SlidingAnalysisResults Construct an instance of this class
            %   Detailed explanation goes here
            sar.slidingAnalysisResults = slidingAnalysisResults;
        end
        
        function [zz_3, D_t_zz_3] = speedPlot(sar)
            %speedPlot Summary of this method goes here
            %   Detailed explanation goes here
            N = length(sar.slidingAnalysisResults);
            
            zz_3 = zeros(N,1);
            D_t_zz_3 = zeros(N,1);
            
            for n = 1:N
                result = sar.slidingAnalysisResults{n};
                zz_3(n) = result.zzLocation();
                if result.isStableSliding()
                    D_t_zz_3(n) = result.slidingSpeed();
                else
                    D_t_zz_3(n) = NaN;
                end
            end
        end
        
        function [flags] = isStableSliding(sar)
            N = length(sar.slidingAnalysisResults);
            flags = false(N,1);
            for n = 1:N
                flags(n) = sar.slidingAnalysisResults{n}.isStableSliding();
            end
        end
        
        function [flags] = isStableCycle(sar)
            N = length(sar.slidingAnalysisResults);
            flags = false(N,1);
            for n = 1:N
                flags(n) = sar.slidingAnalysisResults{n}.isStableCycle();
            end
        end
    end
end

