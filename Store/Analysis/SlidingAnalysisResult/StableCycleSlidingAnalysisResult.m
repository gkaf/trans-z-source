classdef StableCycleSlidingAnalysisResult < SlidingAnalysisResult
    %StableCycleSlidingAnalysisResult Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % finiteTranscriptEvaluation
        D_t_zz_3
        zz_fin
    end
    
    methods
        function [sar] = StableCycleSlidingAnalysisResult(finiteTranscriptEvaluation, zz, dynamicMapsFunctions)
            %StableCycleSlidingAnalysisResult Construct an instance of this class
            %   Detailed explanation goes here
            sar@SlidingAnalysisResult(zz)
            
            % sar.finiteTranscriptEvaluation = finiteTranscriptEvaluation;
            
            D_t_zz_3 = dynamicMapsFunctions.trajectorySlidingSpeed(finiteTranscriptEvaluation);
            sar.D_t_zz_3 = D_t_zz_3;
            
            finalEraTranscript = finiteTranscriptEvaluation.getEraTranscript();
            y_fin = finalEraTranscript.finalContinuumOutput();
            
            sar.zz_fin = y_fin(1:2);
        end
        
        function [finiteTranscriptEvaluation] = getTrajectory(sar)
            finiteTranscriptEvaluation = sar.finiteTranscriptEvaluation;
        end
        
        function [D_t_zz_3] = slidingSpeed(sar)
            %slidingSpeed Summary of this method goes here
            %   Detailed explanation goes here
            D_t_zz_3 = sar.D_t_zz_3;
        end
        
        function [flag] = isStableCycle(~)
            flag = true;
        end
        
        function [flag] = isStableSliding(~)
            flag = true;
        end
        
        function [zz_fin] = finalCyclePoint(sar)
            zz_fin = sar.zz_fin;
        end
    end
end

