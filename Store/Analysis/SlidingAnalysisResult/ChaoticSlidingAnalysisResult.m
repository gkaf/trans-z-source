classdef ChaoticSlidingAnalysisResult < SlidingAnalysisResult
    %ChaoticSlidingAnalysisResult Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % finiteTranscriptEvaluation
        D_t_zz_3
    end
    
    methods
        function [sar] = ChaoticSlidingAnalysisResult(finiteTranscriptEvaluation, zz, dynamicMapsFunctions)
            %ChaoticSlidingAnalysisResult Construct an instance of this class
            %   Detailed explanation goes here
            
            % No need to remove initial points as the burn-in occured
            % during the convergence check!
            
            % % In chaotic transcript remove 1st 2/3 of points as a burn-in
            % % period
            % discreteStateGenerator = finiteTranscriptEvaluation.discreteStateGenerator;
            % steps_left = finiteTranscriptEvaluation.steps_left;
            % ft = FiniteTranscriptEvaluation(discreteStateGenerator, steps_left);
            % 
            % eras = finiteTranscriptEvaluation.getEras();
            % N = length(eras);
            % M = floor((2/3)*N);
            % 
            % for n = M:N
            %     ft = ft.extend(eras{n});
            % end
            
            sar@SlidingAnalysisResult(zz)
            ft = finiteTranscriptEvaluation;
            % sar.finiteTranscriptEvaluation = ft;
            
            D_t_zz_3 = dynamicMapsFunctions.trajectorySlidingSpeed(ft);
            sar.D_t_zz_3 = D_t_zz_3;
        end
        
        function [finiteTranscriptEvaluation] = getTrajectory(sar)
            finiteTranscriptEvaluation = sar.finiteTranscriptEvaluation;
        end
        
        function [D_t_zz_3] = slidingSpeed(sar)
            %slidingSpeed Summary of this method goes here
            %   Detailed explanation goes here
            D_t_zz_3 = sar.D_t_zz_3;
        end
        
        function [flag] = isStableCycle(~)
            flag = false;
        end
        
        function [flag] = isStableSliding(~)
            flag = true;
        end
    end
end

