S_flt_00 = dynamics_functions.filter_points(S, [0;0]);
plot(S_flt_00(1,:), S_flt_00(2,:), '.')
S_flt_10 = dynamics_functions.filter_points(S, [1;0]);
plot(S_flt_10(1,:), S_flt_10(2,:), '.')
S_flt_01 = dynamics_functions.filter_points(S, [0;1]);
plot(S_flt_01(1,:), S_flt_01(2,:), '.')
S_flt_11 = dynamics_functions.filter_points(S, [1;1]);
plot(S_flt_11(1,:), S_flt_11(2,:), '.')