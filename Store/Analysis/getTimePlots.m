function [timePlots] = getTimePlots()
%getTimePlots Summary of this function goes here
%   Detailed explanation goes here

Dt_fin = 0.1;
e_tol = 1e-8;
%abs_tol = e_tol .* ones(size(dynamics_functions.x_ref));
max_step = 1e-5;

Dt_state_ub = 0.2;
Dt_ub = 1.6;
t_0 = 0;
u_0 = [1;1];

timePlots = TimePlots(Dt_fin, Dt_state_ub, Dt_ub, ...
    e_tol, max_step, ...
    t_0, u_0);

end

