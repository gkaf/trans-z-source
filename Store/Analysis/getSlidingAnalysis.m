function [slidingAnalysis] = getSlidingAnalysis()
%getSlidingAnalysis Summary of this function goes here
%   Detailed explanation goes here

dynamicsGenerator = getDynamicsGenerator();

N_surfQ_points = 100;

Dt_cross = 0.4;
Dt_cycle = 1;
e_tol = 1e-8;
max_step = 1e-5;

max_depth = 256;
eta_tolerance = 1e-2;

N_burnin_init = 3200;
N_burnin_semistable = 1200;

Dt_state_ub = 1;
Dt_ub = 6;

t_0 = 0;
u_0 = [1;1];

slidingAnalysis = SlidingAnalysis(dynamicsGenerator, N_surfQ_points, ...
    Dt_cross, Dt_cycle, e_tol, max_step, ...
    max_depth, eta_tolerance, ...
    N_burnin_init, N_burnin_semistable, ...
    Dt_state_ub, Dt_ub, ...
    t_0, u_0);

end

