function [dynamicsGenerator] = getDynamicsGenerator()
%getDynamicsGenerator Summary of this function goes here
%   Detailed explanation goes here

L = 1.5e-3;
n = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;

% i_ref
% v_ref = 480;
r = ones(2,1).*0.01;
r_ESR = 0;
k = 32;
alpha = [384,1];

tolerance = 1e-8;


v_ref = 480;
P_in = 2.4e3;

i_ref = P_in/E_load;
epsilon = 384*i_ref;

dynamicsGenerator = DynamicsGenerator(L, n, L_mag, C, ...
    E_source, E_load, ...
    r, r_ESR, ...
    v_ref, P_in, k, alpha, epsilon, ...
    tolerance);

end

