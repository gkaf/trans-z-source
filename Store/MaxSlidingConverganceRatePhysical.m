classdef MaxSlidingConverganceRatePhysical < MaxSlidingConverganceRate
    %MaxSlidingConverganceRatePhysical Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [sr] = MaxSlidingConverganceRatePhysical(idx_S, zx_fin, u_post, circ)
            %MaxSlidingConverganceRatePhysical Construct an instance of this class
            %   Detailed explanation goes here
            zz_fin = circ.dynamicsFunctionsSurf.normal_space(zx_fin);
            Dt_zz_fin = proj_der(zx_fin, u_post);
            
            sr@MaxSlidingConverganceRate(idx_S, zx_fin, u_post, Dt_zz_fin, zz_fin);
            
            function [dzz_fin] = proj_der(zx_fin, u)
                dzx_fin = circ.dynamicsFunctionsSurf.f_der(zx_fin, u);
                dzz_fin = circ.dynamicsFunctionsSurf.T_trs * dzx_fin;
            end
        end
    end
end

    