classdef ZSourceControlledProj < CircuitProj
    %ZSourceProj Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        U
        num_U
    end
    
    methods
        function [zscp] = ZSourceControlledProj(dynamicsFunctionsSurf1Codim2Surf)
            %ZSourceProj Construct an instance of this class
            %   Detailed explanation goes here
            n_surfaces = 1;
            n_main = 2;
            n_alternative = 3;
            
            zz_1_2_ref = [0; 0];
            zz_3_s = 0;
            
            zscp@CircuitProj(dynamicsFunctionsSurf1Codim2Surf, n_surfaces, n_main, n_alternative, ...
                zz_1_2_ref, zz_3_s);
            
            zscp.U = dynamicsFunctionsSurf1Codim2Surf.U;
            zscp.num_U = length(zscp.U);
        end
        
        function [f] = flow(zsc, u) 
            %flow Summary of this method goes here
            %   Detailed explanation goes here
            f = @der;
            % M = eye(2,2);
            
            fds = zsc.dynamicsFunctionsSurf;
            zz_3 = zsc.zz_3_s;
            function [D_t_zz_1_2] = der(~, zz_1_2) % t, zx
                zx = fds.physical_space([zz_1_2; zz_3]);
                dzz = fds.T_trs*fds.f_der(zx, u);
                D_t_zz_1_2 = dzz(1:2);
            end
        end
        
        function [v] = potential_function(zsc, zx)
            fds = zsc.dynamicsFunctionsSurf;
            zz_1_2 = zx(1:2);
            v = fds.potential_function_proj(zz_1_2);
        end
        
        function [switchingRule_cell] = critical_function(zsc, idx_S, zx_fin, ~) % zsc, idx_S, zx_fin, u
            switchingRule_Stack = Stack();
            idx = 1;
            while idx <= zsc.num_U
                u_post = zsc.idx2ctrl(idx);
                DtS = zsc.dynamicsFunctionsSurf.potential_derivative_proj(zx_fin, u_post, zsc.zz_3_s);
                if DtS < 0
                    zz_1_2_fin = zx_fin;
                    zz_fin = [zz_1_2_fin; zsc.zz_3_s];
                    sr = MaxSlidingConverganceRateProj(idx_S, zx_fin, u_post, zsc, zz_fin);
                    switchingRule_Stack = switchingRule_Stack.push(sr);
                end
                idx = idx+1;
            end
                    
            switchingRule_cell = switchingRule_Stack.toCell();
        end
        
        function [zz] = alternative_space(zsc, zx)
            zz = zsc.dynamicsFunctionsSurf.physical_space([zx; zsc.zz_3_s]);
        end
        
        function [u] = idx2ctrl(zsc, idx)
            u = zsc.U(:, idx);
        end
    end
end

