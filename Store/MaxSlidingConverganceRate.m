classdef (Abstract) MaxSlidingConverganceRate < SwitchingRule
    %MaxConverganceRate Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Dt_z_x_sign_z
    end
    
    methods
        function [sr] = MaxSlidingConverganceRate(idx_S, zx_fin, u_post, Dt_zz_fin, zz_fin)
            %MaxConverganceRate Construct an instance of this class
            %   Detailed explanation goes here
            sr@SwitchingRule(idx_S, zx_fin, u_post);
            sr.Dt_z_x_sign_z = Dt_zz_fin(3)*sign(zz_fin(3));
        end
        
        function [v_crit] = critical_function(sr, ~, ~, ~) % sr, idx_S, zx_fin, u_post
            %critical_function Summary of this method goes here
            %   Detailed explanation goes here
            v_crit = sr.Dt_z_x_sign_z;
        end
    end
end

    