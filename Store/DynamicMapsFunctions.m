classdef DynamicMapsFunctions < DynamicsFunction
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        zz_3_s
        zz_1_2_ref
    end
    
    methods
        function [fds] = DynamicMapsFunctions(L, n, L_mag, C, ...
                E_source, E_load, ...
                r, r_ESR, ...
                i_ref, v_ref, k, alpha,...
                tolerance, ...
                zz_3_s)
            %MapDynamicsFunction Construct an instance of this class
            %   Detailed explanation goes here
            fds@DynamicsFunction(L, n, L_mag, C, ...
                E_source, E_load, ...
                r, r_ESR, ...
                i_ref, v_ref, k, alpha, ...
                tolerance);
            
            fds.zz_3_s = zz_3_s;
            fds.zz_1_2_ref = [0; 0];
        end
        
        
        
        function [zz] = projected_normal_space(fds, zz_1_2)
            zz = [zz_1_2; fds.zz_3_s];
        end
        
        function [S_flt] = filter_projected_points(fds, ZS, u)
            N = size(ZS,2);
            s_fls = Stack();
            for n = 1:N
                zz = ZS(:,n);
                gv = fds.projected_potential_derivative(zz, u);
                if gv < 0
                    s_fls = s_fls.push(zz);
                end
            end
            
            S_flt = cell2mat(s_fls.toCell());
        end
        
        function [Dt_zz_3] = trajectorySlidingSpeed(fds, transcript)
            %trajectorySlidingSpeed Summary of this method goes here
            %   Detailed explanation goes here
            Delta_t = 0;
            Delta_zz_3 = 0;
            
            eras = transcript.getEras();
            N = length(eras);
            for n = 1:N
                if isa(eras{n}, 'FlowEraTranscript')
                    ts = eras{n}.timeInstances();
                    t_start = ts(1);
                    t_end = ts(end);
                    Delta_t_era = t_end - t_start;
                    
                    u = eras{n}.discreteOutput();
                    
                    zz_1_2 = [0; 0];
                    zx = fds.physical_space([zz_1_2; fds.zz_3_s]);
                    dzz = fds.T_trs*fds.f_der(zx, u);
                    Dt_zz_3 = dzz(3);
                    
                    Delta_t = Delta_t + Delta_t_era;
                    Delta_zz_3 = Delta_zz_3 + Dt_zz_3*Delta_t_era;
                end
            end
            
            Dt_zz_3 = Delta_zz_3/Delta_t;
        end
    end
end

