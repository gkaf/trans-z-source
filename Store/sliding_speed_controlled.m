function sliding_speed_controlled(filename)
%sliding_speed Summary of this function goes here
%   Detailed explanation goes here
import_sliding_analysis

ZZ_3_s = -120:0.1:120;
slidingAnalysis = getSlidingAnalysisZSourceControlledProj();

slidingAnalysisResults = slidingAnalysis.slidingSpeed(ZZ_3_s);

save_results(slidingAnalysisResults, filename);

path(pathdef);
end

