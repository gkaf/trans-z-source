% z-source/simulation.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

[dynamics_functions, epsilon] = getDynamicsFunction1Codim2Surf();

circuit = ZSource(dynamics_functions);

Dt_cross = 0.0002;
Dt_period = 0.02;
e_tol = 1e-12;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;
convergence_tolerance = 0.5e-3;
stateGenerator = SRCDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, convergence_tolerance, ...
    Dt_cross, Dt_period);

ha = HybridAutomaton(stateGenerator);

N_ub = 32;
Dt_burn_in = 0.04;
burnInState = BurnInState(Dt_burn_in);

Dt_state_ub = 2*Dt_cross;
Dt_ub = N_ub*Dt_period + Dt_burn_in;
t_0 = 0;
x_0 = circuit.nominal_state();
u_0 = [1;1];

discreteStateData = burnInState;
initial_stateId = StateId(SRCDetectionDiscreteStateEnumeration.StartSRConvergenceDetection, discreteStateData, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

meanStates = hae.evaluate();

