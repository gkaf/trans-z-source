classdef MaxSlidingConverganceRateProj < MaxSlidingConverganceRate
    %MaxSlidingConverganceRateProj Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [sr] = MaxSlidingConverganceRateProj(idx_S, zx_fin, u_post, circ, zz_fin)
            %MaxSlidingConverganceRateProj Construct an instance of this class
            %   Detailed explanation goes here
            zx = circ.dynamicsFunctionsSurf.physical_space(zz_fin);
            Dt_zz_fin = proj_der(zx, u_post);
            
            sr@MaxSlidingConverganceRate(idx_S, zx_fin, u_post, Dt_zz_fin, zz_fin);
            
            function [dzz_fin] = proj_der(zx_fin, u)
                dzx_fin = circ.dynamicsFunctionsSurf.f_der(zx_fin, u);
                dzz_fin = circ.dynamicsFunctionsSurf.T_trs * dzx_fin;
            end
        end
    end
end

    