function analyze_sliding_speed(filename)
import_analysis

slidingAnalysis = getSlidingAnalysis();

ZZ_3_s = -120:0.1:120;
slidingAnalysisResults = slidingAnalysis.slidingSpeed(ZZ_3_s);

[zz_3, D_t_zz_3] = slidingAnalysisResults.speedPlot();
stable_sliding = slidingAnalysisResults.isStableSliding();
stable_cycle = slidingAnalysisResults.isStableCycle();

dlmwrite(filename, [zz_3, D_t_zz_3, stable_sliding, stable_cycle], '\t');

path(pathdef);
end
