% z-source/simulation.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

[dynamics_functions, epsilon] = getDynamicsFunction1Codim2SurfFlat();

circuit = ZSource(dynamics_functions);

Dt_fin = 0.1*1;
e_tol = 1e-8;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;
state_gen = TransZSourceDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, ...
    Dt_fin);

ha = HybridAutomaton(state_gen);

Dt_state_ub = 0.2;
Dt_ub = 1.6;
t_0 = 0;
x_0 = circuit.nominal_state();
% Selecting initial state on the sliding mode:
% x_0 = circuit.dynamicsFunctionsSurf.physical_space([0;0;40]);
discreteStateData = Transcript(state_gen);
u_0 = [1;1];

initial_stateId = StateId(TransZSourceDiscreteStateEnumeration.Initial, discreteStateData, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

transcript = hae.evaluate();
 
[ts, ys, us] = transcript.get_tyu();

% N = floor(length(ts)*(2/3));
% D_ST = us(1,N:end).*us(2,N:end);
% D_st = mean(D_ST);
% M = us(1,N:end) - us(2,N:end);
% m = mean(M);

