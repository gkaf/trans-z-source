function [Di_L_m] = analytical_sliding_speed(k, i_L_m)
%analytical_sliding_speed Summary of this function goes here
%   Detailed explanation goes here

E_1 = 100;
E_2 = 380;
C = 48e-6;
n = 2;
L_m = 1e-3;
i_L_s = 2.4e3/E_2;
v_C_s = 480;

E = [E_1; E_2];
D_s = (v_C_s - E(1))/(v_C_s*(n+1) - E(1));
M = (E(2)/E(1))*(1-(n+1)*D_s);
i_L_m_s = M*i_L_s*(1+1/n) / ((1 - D_s)/n - D_s);

Alpha = k^2/L_m + 1/C;
Beta = k*v_C_s/L_m - i_L_m_s/C;
Gamma = n*v_C_s + (v_C_s - E_1);
Delta = i_L_m_s/C;

D = (-(1/n)*Alpha*(i_L_m - i_L_m_s) + (n+1)*E_2/(-(n+1)*k*(i_L_m - i_L_m_s) + Gamma) - Delta/n) / (-(n+1)*Alpha*(i_L_m - i_L_m_s)/n + (n+1)*Beta/n);
del_D = (-(1/n)*Alpha + ((n+1)^2*k*E_2)/((-(n+1)*k*(i_L_m - i_L_m_s) + Gamma)^2)) / (-(n+1)*Alpha*(i_L_m - i_L_m_s)/n + (n+1)*Beta/n) + ...
    (-((n+1)/n^2)*Alpha^2*(i_L_m - i_L_m_s) + ((n+1)^2*Alpha/n)*(E_2/(-(n+1)*k*(i_L_m - i_L_m_s) + Gamma)) + (n+1)*Alpha*Delta/n^2) / (-(n+1)*Alpha*(i_L_m - i_L_m_s)/n + (n+1)*Beta/n)^2;
Di_L_m = (1/L_m)*((k/n)*(1 - (n+1)*D) + (k/n)*del_D*(i_L_m - i_L_m_s) + (n+1)*del_D*v_C_s/n);
end

