classdef ZSourceControlled < Circuit
    %ZSource Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        U
        num_U
    end
    
    methods
        function [zsc] = ZSourceControlled(dynamicsFunctionsSurf1Codim2Surf)
            %ZSource Construct an instance of this class
            %   Detailed explanation goes here
            n_surfaces = 1;
            n_main = 3;
            n_alternative = 3;
            zsc@Circuit(dynamicsFunctionsSurf1Codim2Surf, n_surfaces, n_main, n_alternative);
            
            zsc.U = [[0;0], [1;0], [0;1], [1;1]];
            zsc.num_U = length(zsc.U);
        end
        
        function [f] = flow(zsc, u) 
            %flow Summary of this method goes here
            %   Detailed explanation goes here
            fds = zsc.dynamicsFunctionsSurf;
            f = @der;
            % M = eye(3,3);
            
            function [dx] = der(~, zx) % t, zx
                dx = fds.f_der(zx, u);
            end
        end
        
        function [v] = potential_function(zsc, zx)
            fds = zsc.dynamicsFunctionsSurf;
            v = fds.potential_function(zx);
        end
        
        function [switchingRule_cell] = critical_function(zsc, idx_S, zx_fin, ~) % zsc, idx_S, zx_fin, u
            switchingRule_Stack = Stack();
            idx = 1;
            while idx <= zsc.num_U
                u_post = zsc.idx2ctrl(idx);
                DtS = zsc.dynamicsFunctionsSurf.potential_derivative(zx_fin, u_post);
                if DtS < 0
                    sr = MaxSlidingConverganceRatePhysical(idx_S, zx_fin, u_post, zsc);
                    switchingRule_Stack = switchingRule_Stack.push(sr);
                end
                idx = idx+1;
            end
                    
            switchingRule_cell = switchingRule_Stack.toCell();
        end
        
        function [zz] = alternative_space(zsc, zx)
            zz = zsc.dynamicsFunctionsSurf.normal_space(zx);
        end
        
        function [u] = idx2ctrl(zsc, idx)
            u = zsc.U(:, idx);
        end
        
        function [x] = nominal_state(zsc)
            x = zsc.dynamicsFunctionsSurf.zx_ref;
        end
    end
end

