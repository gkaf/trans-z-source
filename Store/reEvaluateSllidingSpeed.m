function [D_t_Z] = reEvaluateSllidingSpeed(circuitProj, Z, U)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

N = length(Z);
D_t_Z = zeros(N,1);

for n = 1:N
    zz_3 = Z(n);
    u = transpose(U(n,:));
    circuitProj = circuitProj.reset_zz_3_s(zz_3);
    D_t_Z(n) = circuitProj.averaged_sliding_speed(u);
end

end

