classdef StepEra < ExtendedMapFunctions & DiscreteStateFlow
    %StepPeriod Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        epsilon
        eta
        % Info to detect unstable sliding surface
        Dt_fin
        % Stack of crossing points
        crossing_points
    end
    
    methods
        function [ds] = StepEra(u, ...
                mapDynamicsFunction, ...
                epsilon, eta, ...
                Dt_fin, ...
                flowIntegrator, ...
                continuum_output_size)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            
            ds@ExtendedMapFunctions(mapDynamicsFunction);
            ds@DiscreteStateFlow(TransZSourceDiscreteStateEnumeration.Loop, u, ...
                flowIntegrator, Interval.Trajectory, ...
                continuum_output_size);
            
            ds.epsilon = epsilon;
            ds.eta = eta;
            ds.Dt_fin = Dt_fin;
            
            ds.crossing_points = Stack();
        end
        
        function [f, M] = flow(ds, ~ , ~) % ds, t, x
            %flow Summary of this method goes here
            %   Detailed explanation goes here
            n = ds.continuumOutputSize() - ds.compressedContinuumOutputSize();
            M = eye(n,n);
            
            f = ds.mapDynamicsFunction.projected_flow(ds.u);
        end
        
        function [e, idx_terminal_bitmap] = event(ds, ~, x)
            % u = [u_1; u_2]
            if ismember(ds.u(1), [0,1]) && ismember(ds.u(2), [0,1])
                e = getEventsFcn();
            else
                e = {};
            end
            idx_terminal_bitmap = [1; 1];
            
            eps = ds.epsilon;
            t_init = ds.starting_time(x);
            Dt_max = ds.Dt_fin;
            
            function [e] = getEventsFcn()
                e = @eventsFcn;
                
                function [position, isterminal, direction] = eventsFcn(t, zx)
                    v = ds.dynamicsFunctions.potential_function(zx);
                    position = [ v - eps; ...
                        Dt_max - (t - t_init)]; % The value that we want to be zero
                    isterminal = idx_terminal_bitmap; % Halt integration
                    direction = [1; -1];
                end
            end
        end
        
        function [stateId] = transition(ds, e_idx, ~, x)
            discreteStateEnumeration = TransZSourceDiscreteStateEnumeration.Loop;
            u = ds.u;
            
            k = 1;
            wrong_idx = false;
            wrong_signal = false;
            while k <= length(e_idx) && ~(wrong_idx || wrong_signal)
                if e_idx(k) == 1
                    
                    if false
                        % if crossing detected, finalize the search and send the
                        % approriate number of eras to be evaluated.
                    else
                        zx = ds.map(x);
                        ds.crossing_points
                        
                        %s_crit = Stack();
                        s_ds = Stack();
                        s_idx = Stack();
                        n = 1;
                        while n <= ds.num_U
                            u = ds.idx2ctrl(n);
                            gv = ds.dynamicsFunctions.directional_derivative(zx, u);
                            if gv < 0
                                %v_crit = ds.dynamicsFunctions.critical_function(xz);
                                %s_crit = s_crit.push(v_crit);
                                s_ds = s_ds.push(gv);
                                s_idx = s_idx.push(n);
                            end
                            n = n+1;
                        end
                        
                        %s_crit = cell2mat(s_crit.toCell());
                        s_ds = cell2mat(s_ds.toCell());
                        s_idx = cell2mat(s_idx.toCell());
                        
                        [~, n] = min(s_ds);
                        idx = s_idx(n);
                        u = ds.idx2ctrl(idx);
                    end
                    
                elseif e_idx(k) == 2
                    discreteStateEnumeration = TransZSourceDiscreteStateEnumeration.Final;
                else
                    wrong_idx = true;
                end
                
                k = k+1;
            end
            
            if wrong_idx || wrong_signal
                stateId = {};
            else
                stateId = StateId(discreteStateEnumeration, u);
            end
        end
    end
end

