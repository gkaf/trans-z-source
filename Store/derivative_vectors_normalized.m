% z-source/simulation.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

L = 1.5e-3;
n = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;
i_ref = 2.4e3/E_load;
v_ref = 480;
r = ones(2,1).*0.01;
r_ESR = 0;
k = 32;
alpha = [384,1];

epsilon = 384*i_ref;

tolerance = 1e-8;
dynamics_functions = DynamicsFunction(L, n, L_mag, C,...
    E_source, E_load, ...
    r, r_ESR, ...
    i_ref, v_ref, k, alpha, ...
    tolerance);

U = {[0;0], [1;0], [0;1], [1;1]};
dynamics_functions.T_trs*dynamics_functions.f_der(dynamics_functions.x_ref, U{1})
dynamics_functions.T_trs*dynamics_functions.f_der(dynamics_functions.x_ref, U{2})
dynamics_functions.T_trs*dynamics_functions.f_der(dynamics_functions.x_ref, U{3})
dynamics_functions.T_trs*dynamics_functions.f_der(dynamics_functions.x_ref, U{4})
