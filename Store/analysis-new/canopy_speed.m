function [m, D] = canopy_speed(k, i_L_m, ...
    E, ...
    n_turns, C, L_m, ...
    i_L_s, i_L_m_s, v_C_s)
%sliding_speed Summary of this function goes here
%   Detailed explanation goes here

v_C_sliding = v_C_s - k*(i_L_m - i_L_m_s);

m = E(2)/(v_C_sliding + (1/n_turns)*(v_C_sliding - E(1)));
%D = ((k/L_m)*(v_C_sliding - E(1)) - i_L_m/C + (m*(n+1)/C)*i_L_s) / ((k/L_m)*((n+1)*v_C_sliding - E(1)) - ((n+1)/C)*i_L_m);
D = ( (k/L_m)*(v_C_sliding - E(1)) + (1/C)*(m*(n_turns+1)*i_L_s - i_L_m) ) / ( (k/L_m)*((n_turns+1)*v_C_sliding - E(1)) - (1/C)*(n_turns+1)*i_L_m );

%Di_L_m = (1/L_m)*(((1-D)/n)*(E(1) - v_C_sliding) + D*v_C_sliding);
end

