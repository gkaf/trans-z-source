function [ZZ_3_s_feas,u_can] = canopy(r, ZZ_3_s)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

tolerance = 1e-8;
options = optimoptions('fsolve', 'Display', 'off', 'FunctionTolerance', tolerance);

L = 1.5e-3;
n = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;
i_ref = 2.4e3/E_load;
v_ref = 480;
r_ESR = 0;
k = 32;

dynf = DynamicsFunctionNonIdeal(L, n, L_mag, C, ...
    E_source, E_load, ...
    r, r_ESR, ...
    i_ref, v_ref, k);

N = length(ZZ_3_s);

U = [[0;0], [1;0], [0;1], [1;1]];

ZZ_3_s_feas_stack = Stack();
u_can_stack = Stack();

for n = 1:N
    zz_3_s = ZZ_3_s(n);
    x_0  = dynf.physical_space([0; 0; zz_3_s]);
    f = get_dynamics(x_0);
    
    u_case_stack = Stack();
    for n_u = 1:size(U,2)
        u_0 = U(:,n_u);
        [u_can,~,exitflag,~,~] = fsolve(f,u_0,options); % x,fval,exitflag,output,jacobian
        equation_not_solved = exitflag <=0 ;
        if ~equation_not_solved
            u_case_stack = u_case_stack.push(u_can);
        end
    end
    
    if ~u_case_stack.isEmpty()
        u_case = u_case_stack.toCell();
        
        u_can_stack = u_can_stack.push(u_case);
        ZZ_3_s_feas_stack = ZZ_3_s_feas_stack.push(zz_3_s);
    end
end

u_can = u_can_stack.toCell();
ZZ_3_s_feas = cell2mat(ZZ_3_s_feas_stack.toCell());

    function [f] = get_dynamics(x_0)
        f = @dyn;
        function [dx] = dyn(u)
            dx = dynf.f_der([], x_0, u);
        end
    end
end

