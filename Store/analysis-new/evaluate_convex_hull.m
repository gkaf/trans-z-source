function evaluate_convex_hull(filename)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
r = [0.01; 0.01];
ZZ_3_s = -120:0.1:120;

[f_sld_min, gamma_min, f_sld_max, gamma_max, ~, ZZ_3_s_feas] = convex_hull(r, ZZ_3_s);

f_sld_min = transpose(f_sld_min);
gamma_min = transpose(gamma_min);
f_sld_max = transpose(f_sld_max);
gamma_max = transpose(gamma_max);
ZZ_3_s_feas = transpose(ZZ_3_s_feas);

mean_f_sld = (f_sld_min + f_sld_max)/2;
diff_f_sld = f_sld_max + f_sld_min;

T = table(ZZ_3_s_feas, f_sld_min, gamma_min(:,1), gamma_min(:,2), gamma_min(:,3), gamma_min(:,4), ...
    f_sld_max, gamma_max(:,1), gamma_max(:,2), gamma_max(:,3), gamma_max(:,4), ...
    mean_f_sld, diff_f_sld, ...
    'VariableNames', {'z', 'f_sld_min', 'gamma_min_00', 'gamma_min_10', 'gamma_min_01', 'gamma_min_11', ...
    'f_sld_max', 'gamma_max_00', 'gamma_max_10', 'gamma_max_01', 'gamma_max_11', ...
    'mean_f_sld', 'diff_f_sld'});

writetable(T,filename,'WriteVariableNames',true,'Delimiter',';');

end

