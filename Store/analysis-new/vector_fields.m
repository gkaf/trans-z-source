function [dx, U, T_proj, Delta, V] = vector_fields(r, zz_3_s)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

L = 1.5e-3;
n = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;
i_ref = 2.4e3/E_load;
v_ref = 480;
r_ESR = 0;
k = 32;

tolerance = 1e-8;
dynf = DynamicsFunction(L, n, L_mag, C, ...
    E_source, E_load, ...
    r, r_ESR, ...
    i_ref, v_ref, k, ...
    tolerance);

U = [[0;0],[1;0],[0;1],[1;1]];
x_0  = dynf.physical_space([0; 0; zz_3_s]);

dx = zeros(3,4);
dx(:,1) = dynf.f_der(x_0, U(:,1));
dx(:,2) = dynf.f_der(x_0, U(:,2));
dx(:,3) = dynf.f_der(x_0, U(:,3));
dx(:,4) = dynf.f_der(x_0, U(:,4));

Delta = [dx(:,1) - dx(:,2), dx(:,1) - dx(:,3), dx(:,1) - dx(:,4)];

V = (1/2)*det(Delta);

T_proj = dynf.T_trs;

end

