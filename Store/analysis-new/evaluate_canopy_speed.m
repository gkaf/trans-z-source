function [ZZ, Dzz, u, mD] = evaluate_canopy_speed(ZZ)

E_1 = 100;
E_2 = 380;
L = 1.5e-3;
C = 48e-6;
n_turn = 2;
L_m = 1e-3;
i_L_s = 2.4e3/E_2;
v_C_s = 480;

E = [E_1; E_2];
D_s = (v_C_s - E(1))/(v_C_s*(n_turn+1) - E(1));
m_s = (E(2)/E(1))*(1-(n_turn+1)*D_s);
i_L_m_s = m_s*i_L_s*(1+1/n_turn) / ((1 - D_s)/n_turn - D_s);

k = 32;
r = [0;0];
r_ESR = 0;

tolerance = 1e-6;
dyn = DynamicsFunction(L, n_turn, L_m, C, ...
    E_1, E_2, ...
    r, r_ESR, ...
    i_L_s, v_C_s, k, ...
    tolerance);

Dzz = zeros(size(ZZ));
u = zeros(2,length(ZZ));
mD = zeros(2,length(ZZ));

for idx = 1:length(ZZ)
    zz_3 = ZZ(idx);
    zx = dyn.physical_space([0;0;zz_3]);
    i_L_m = zx(2);
    [m, D] = canopy_speed(k, i_L_m, ...
        E, ...
        n_turn, C, L_m, ...
        i_L_s, i_L_m_s, v_C_s);
    
    mD(:,idx) = [m;D];
    u(:,idx) = (1/2).*([m; -m] + ((m^2 + 4*D)^(1/2)).*[1;1]);
    
    zx = dyn.physical_space([0;0; zz_3]);
    Dz_123 = dyn.T_trs*dyn.f_der(zx, u(:,idx));
    Dz_123(1)
    Dz_123(2)
    Dzz(idx) = Dz_123(3);
end

flt_0 = u(1,:) > 0 & u(2,:) > 0;
flt_1 = u(1,:) < 1 & u(2,:) < 1;
flt = flt_0 & flt_1;

ZZ = ZZ(flt);
Dzz = Dzz(flt);
u = u(:,flt);
mD = mD(:,flt);

end