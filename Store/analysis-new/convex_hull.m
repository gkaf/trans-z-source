function [f_sld_min, gamma_min, f_sld_max, gamma_max, U_ord, ZZ_3_s_feas] = convex_hull(r, ZZ_3_s)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%options = optimoptions('linprog','Algorithm','interior-point','Display','off','MaxIterations', 2000);
options = optimoptions('linprog','Algorithm','dual-simplex','Display','off');


N = length(ZZ_3_s);

U_ord_stack = Stack();
ZZ_3_s_feas_stack = Stack();
f_sld_min_stack = Stack();
gamma_min_stack = Stack();
f_sld_max_stack = Stack();
gamma_max_stack = Stack();

for n = 1:N
    zz_3_s = ZZ_3_s(n);
    [dx, U, T_proj, ~, ~] = vector_fields(r, zz_3_s);
    
    proj_3 = T_proj(3,:);
    proj_1_2 = T_proj(1:2,:);
    
    f = transpose(proj_3*dx);
    A = [-eye(4,4);eye(4,4)];
    b = [zeros(4,1);ones(4,1)];
    Aeq = [proj_1_2*dx; ones(1,4)];
    beq = [0;0;1];
    %lb = zeros(4,1);
    %ub = ones(4,1);
    
    %[x,fval,exitflag,~,~] = linprog(f,A,b,Aeq,beq,lb,ub,options); % x,fval,exitflag,output,lambda
    [x,fval,exitflag,~,~] = linprog(f,A,b,Aeq,beq,[],[],options); % x,fval,exitflag,output,lambda
    no_feasible_point_was_found = exitflag == -2;
    problem_infeasible = exitflag == -5;
    if ~(no_feasible_point_was_found || problem_infeasible)
        gamma_min_stack = gamma_min_stack.push(x);
        f_sld_min_stack = f_sld_min_stack.push(fval);
        
        f = -f;
        [x,fval,~,~,~] = linprog(f,A,b,Aeq,beq,[],[],options); % x,fval,exitflag,output,lambda
        gamma_max_stack = gamma_max_stack.push(x);
        fval = -fval;
        f_sld_max_stack = f_sld_max_stack.push(fval);
        
        U_ord_stack = U_ord_stack.push(U);
        ZZ_3_s_feas_stack = ZZ_3_s_feas_stack.push(zz_3_s);
    end
end

gamma_min = cell2mat(gamma_min_stack.toCell());
f_sld_min = cell2mat(f_sld_min_stack.toCell());
gamma_max = cell2mat(gamma_max_stack.toCell());
f_sld_max = cell2mat(f_sld_max_stack.toCell());
U_ord = U_ord_stack.toCell();
ZZ_3_s_feas = cell2mat(ZZ_3_s_feas_stack.toCell());
end

