        function [zz_3] = project_into_normal_3(fds, v_C)
            zz_3_0 = 0;
            [zz_3, ~, ~, ~] = fsolve(@f, zz_3_0, fds.solver_options); % x, fval, exitflag, output
            
            function [Delta] = f(zz_3)
                zz_1_2 = [0;0];
                zx = fds.physical_space([zz_1_2; zz_3]);
                zx_3 = zx(3);
                Delta = zx_3 - v_C;
            end
        end