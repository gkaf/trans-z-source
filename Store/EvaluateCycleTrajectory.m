classdef EvaluateCycleTrajectory < ExtendedMapFunctions & DiscreteStateFlow
    %EvaluateTrajectory Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n_cycle_eras
    end
    
    methods
        function [ds] = EvaluateCycleTrajectory(u, ...
                mapDynamicsFunction, ...
                n_cycle_eras, ...
                flowIntegrator, ...
                continuum_output_size)
            %UNTITLED5 Construct an instance of this class
            %   Detailed explanation goes here
            
            ds@ExtendedMapFunctions(mapDynamicsFunction);
            ds@DiscreteStateFlow(TransZSourceDiscreteStateEnumeration.Loop, u, ...
                flowIntegrator, Interval.Trajectory, ...
                continuum_output_size);
            
            ds.n_cycle_eras = n_cycle_eras;
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

