classdef MapTransZSourceDiscreteStateEnumeration < DiscreteStateEnumeration
    %FilterDiscreteStateEnumeration Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/FilterDiscreteStateEnumeration.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    enumeration
        % Cycle detection
        %InitializeCycleDetection
        %StepDetectPeriod
        %CheckConvergence
        %InitializeTrajectoryEvaluation
        %StepEvaluatePeriod
        %FinalizeTrajectoryEvaluation
        TestInner
    end
    
    methods
        function [fdse] = MapTransZSourceDiscreteStateEnumeration()
            fdse@DiscreteStateEnumeration();
        end
    end
    
end

