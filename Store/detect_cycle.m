% z-source/simulation.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

[dynamics_functions, epsilon] = getDynamicsFunction1Codim2Surf();

circuit = ZSource(dynamics_functions);

Dt_cross = 0.4;
Dt_cycle = 1;
e_tol = 1e-8;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;
stateGenerator = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, ...
    Dt_cross, Dt_cycle);

ha = HybridAutomaton(stateGenerator);

max_depth = 256;
eta_tolerance = 1e-2;
N_burnin = 3200;
trajectoryCheckPoints = TrajectoryCheckPoints(max_depth, eta_tolerance);
burnInState = BurnInState(trajectoryCheckPoints, N_burnin);

Dt_state_ub = 4*Dt_cross;
Dt_ub = 4*Dt_cycle;
t_0 = 0;
x_0 = circuit.nominal_state();
u_0 = [1;1];

discreteStateData = burnInState;
initial_stateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, discreteStateData, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

cycleAnalysisResult = hae.evaluate();

