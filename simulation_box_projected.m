% z-source/simulation_box_projected.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

import_flow;

[dynamicsFunction, switchingFunction2Codim1, epsilon] = getDynamicsFunction2Codim1Surf();
circuit = ZSourceBoxProj(dynamicsFunction, switchingFunction2Codim1);

zz_3_s = 0; % 0 -> chaos?, 40 -> large period, 60 -> small period
circuit = circuit.reset_sliding_state(zz_3_s);

Dt_fin = 0.008;
Dt_burn_in = 0.04;

e_tol = 1e-12;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;
state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, ...
    Dt_fin, Dt_burn_in);

ha = HybridAutomaton(state_gen);

Dt_state_ub = 0.02;
Dt_ub = 1.6;
t_0 = 0;
x_0 = circuit.nominal_state();

discreteStateData = Transcript(state_gen);
u_0 = [1;1];

initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

transcript = hae.evaluate();
 
[ts, ys, us] = transcript.get_tyu();

path(pathdef);

% N = floor(length(ts)*(2/3));
% D_ST = us(1,N:end).*us(2,N:end);
% D_st = mean(D_ST);
% M = us(1,N:end) - us(2,N:end);
% m = mean(M);

