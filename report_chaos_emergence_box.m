function [T_m5, T_0, T_p10] = report_chaos_emergence_box(root_fld)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

result_fld = [root_fld, '/data/trans-Z-source/limit-cycle-analysis/sliding-variable/box/chaos'];

zz_3_s = -5;
Dt_fin = 0.001;
[ts, ys, ~] = evaluate_trajecotry(zz_3_s, Dt_fin);

idx_crit = floor((2/3)*length(ts));
ys = ys(:, 1:idx_crit);

T_m5 = table(ys(1,:)', ys(2,:)', ...
    'VariableNames', {'h_1', 'h_2'});

if ~isempty(root_fld)
    filename = [result_fld, '/', 'phaseplot_m5.dat'];
    writetable(T_m5, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

zz_3_s = 0;
Dt_fin = 0.002;
[ts, ys, ~] = evaluate_trajecotry(zz_3_s, Dt_fin);

idx_crit = floor((2/3)*length(ts));
ys = ys(:, 1:idx_crit);

T_0 = table(ys(1,:)', ys(2,:)', ...
    'VariableNames', {'h_1', 'h_2'});

if ~isempty(root_fld)
    filename = [result_fld, '/', 'phaseplot_0.dat'];
    writetable(T_0, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

zz_3_s = 10;
Dt_fin = 0.001;
[ts, ys, ~] = evaluate_trajecotry(zz_3_s, Dt_fin);

idx_crit = floor((2/3)*length(ts));
ys = ys(:, 1:idx_crit);

T_p10 = table(ys(1,:)', ys(2,:)', ...
    'VariableNames', {'h_1', 'h_2'});

if ~isempty(root_fld)
    filename = [result_fld, '/', 'phaseplot_p10.dat'];
    writetable(T_p10, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

    function [ts, ys, us] = evaluate_trajecotry(zz_3_s, Dt_fin)
        import_flow;
        
        [dynamicsFunction, switchingFunction2Codim1, epsilon] = getDynamicsFunction2Codim1Surf();
        circuit = ZSourceBoxProj(dynamicsFunction, switchingFunction2Codim1);
        
        circuit = circuit.reset_sliding_state(zz_3_s);
        
        Dt_burn_in = 0.02;
        
        e_tol = 1e-12;
        rel_tol = e_tol;
        abs_tol = e_tol .* ones(size(circuit.nominal_state()));
        max_step = 1e-5;
        state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
            circuit, ...
            epsilon, ...
            Dt_fin, Dt_burn_in);
        
        ha = HybridAutomaton(state_gen);
        
        Dt_state_ub = 0.02;
        Dt_ub = 1.6;
        t_0 = 0;
        x_0 = circuit.nominal_state();
        
        discreteStateData = Transcript(state_gen);
        u_0 = [1;1];
        
        initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
        hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);
        
        transcript = hae.evaluate();
        
        [ts, ys, us] = transcript.get_tyu();
        
        path(pathdef);
    end

end

