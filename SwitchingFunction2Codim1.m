classdef SwitchingFunction2Codim1 < SwitchingFunction & DynamicsFunctionIdeal
    %SwitchingFunction2Codim1 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        S
        D
        R
    end
    
    methods
        function [sf] = SwitchingFunction2Codim1(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k,...
                S)
            %SwitchingFunction2Codim1 Construct an instance of this class
            %   Detailed explanation goes here
            sf@SwitchingFunction();
            sf@DynamicsFunctionIdeal(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k);
            sf.S = S;
            
            zx_s = sf.zx_ref;
            values = [0,1];
            U = zeros(2,length(values)^2);
            D_t_S = zeros(2,length(values)^2);
            idx = 1;
            for u_1 = values
                for u_2 = values
                    u = [u_1; u_2];
                    
                    f = sf.f_der([], zx_s, u);
                    grad_potential = sf.S*sf.J_H;
                    D_t_S(:,idx) = grad_potential*f;
                    
                    U(:,idx) = u;
                    
                    idx = idx + 1;
                end
            end
            
            D = U == true;
            
            R = D_t_S > 0;
            
            sf.D = D;
            sf.R = R;
        end
        
        function [u] = vector_field(df, w)
            N = size(df.D, 1);
            u = zeros(N,1);
            
            N_case = size(df.D, 2);
            for idx_u = 1:N
                for idx_case = 1:N_case
                    if df.D(idx_u,idx_case)
                        u(idx_u) = u(idx_u) + ...
                            w(1)*w(2)*df.R(1,idx_case)*df.R(2,idx_case) + ...
                            (1-w(1))*w(2)*(1-df.R(1,idx_case))*df.R(2,idx_case) + ...
                            w(1)*(1-w(2))*df.R(1,idx_case)*(1-df.R(2,idx_case)) + ...
                            (1-w(1))*(1-w(2))*(1-df.R(1,idx_case))*(1-df.R(2,idx_case));
                    end
                end
            end
        end
        
        function [v] = potential_function(fds, ~, h)
            % h : the value of the switching function
            s = fds.S*h;
            v = s.^2;
        end
        
        function [D_t_V] = potential_derivative(fds, t, zx, h, u)
            f = fds.f_der(t, zx, u);
            s = fds.S*h;
            grad_potential = fds.S*fds.J_H;
            D_t_V = (2.*s).*(grad_potential*f);
        end
    end
    
    methods (Static)
        function [switchingFunction2Codim1,S,solutions] = default_generate(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k)
            
            epsilon_opt = 1e-8;
            epsilon_constr = 1e-8;
            epsilon_step = 1e-12;
            n_iter_max = 200;
            
            [switchingFunction2Codim1,S,solutions] = SwitchingFunction2Codim1.generate(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k, ...
                epsilon_opt, epsilon_constr, epsilon_step, n_iter_max);
        end
        
        function [switchingFunction2Codim1,S,solutions] = generate(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k, ...
                epsilon_opt, epsilon_constr, epsilon_step, n_iter_max)
            
            options = optimoptions(@quadprog, 'Algorithm', 'interior-point-convex', 'Display','off', ...
                'OptimalityTolerance', epsilon_opt, 'ConstraintTolerance', epsilon_constr, ...
                'StepTolerance', epsilon_step, 'MaxIterations', n_iter_max);
            
            df = DynamicsFunctionIdeal(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k);
            
            U = control_vectors();
            membership_vectors = combinations(2,4);
            N_constr = floor(size(membership_vectors,2)/2);
            %N_constr = size(membership_vectors,2);
            
            solution_stack = Stack();
            H = eye(2,2);
            f = zeros(2,1);
            Aeq = []; beq = []; lb = []; ub = []; x0 = [];
            n_constr = 1;
            while n_constr <= N_constr
                [A,b] = constraints(n_constr, U, membership_vectors);
                [v,fval,exitflag] = quadprog(H,f,A,b,Aeq,beq,lb,ub,x0,options);
                
                if exitflag == 1 % Solution exists
                    solution.v = v;
                    solution.membership_vectors = membership_vectors(:,n_constr);
                    solution.fval = fval;
                    solution.exitflag = exitflag;
                    
                    solution_stack = solution_stack.push(solution);
                end
                
                n_constr = n_constr + 1;
            end
            
            solutions = solution_stack.toCell();
            N_surfaces = solution_stack.size();
            S = zeros(2,N_surfaces);
            for n_surf = 1:N_surfaces
                v = solutions{n_surf}.v;
                S(:,n_surf) = v./norm(v,2);
            end
            S = transpose(S);
            
            switchingFunction2Codim1 = SwitchingFunction2Codim1(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k,...
                S);
                        
            function [U] = control_vectors()
                values = [0,1];
                U = zeros(2,length(values)^2);
                idx = 1;
                for u_1 = values
                    for u_2 = values
                        u = [u_1; u_2];
                        U(:,idx) = u;
                        idx = idx + 1;
                    end
                end
            end
            
            function [A,b] = constraints(k, U, membership_vectors) 
                N = size(U,2);
                
                Y = zeros(N,1);
                A = zeros(N,size(U,1));
                for n = 1:N
                    idx = membership_vectors(n,k);
                    if idx > 0
                        Y(n) = 1;
                    else
                        Y(n) = -1;
                    end
                    u = U(:,n);
                    dx = df.f_der([], df.zx_ref, u);
                    
                    dzz = df.J_H*dx;
                    
                    A(n,:) = Y(n).*transpose(dzz);
                end
                b = ones(N,1);
                
                A = -A.*1e-6; % Scale condition matrix for better numerical performance
                b = -b;
            end
            
            function [U_idx] = combinations(k,n)
                %combinations Summary of this function goes here
                %   Detailed explanation goes here
                
                if k == 0
                    U_idx = zeros(n,1);
                elseif k == n
                    U_idx = ones(n,1);
                else
                    sf_0 = combinations(k, n-1);
                    sf_1 = combinations(k-1, n-1);
                    
                    M = size(sf_0, 2) + size(sf_1, 2);
                    U_idx = zeros(n, M);
                    
                    m_U = 1;
                    m = 1;
                    while m <= size(sf_0, 2)
                        U_idx(:,m_U) = [0; sf_0(:,m)];
                        m = m + 1;
                        m_U = m_U + 1;
                    end
                    m = 1;
                    while m <= size(sf_1, 2)
                        U_idx(:,m_U) = [1; sf_1(:,m)];
                        m = m + 1;
                        m_U = m_U + 1;
                    end
                end
                
            end
        end
    end
end

