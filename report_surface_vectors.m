function [D,R,S] = report_surface_vectors(fld)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[~, switchingFunction2Codim1, ~] = getDynamicsFunction2Codim1Surf();

D = switchingFunction2Codim1.D;
R = switchingFunction2Codim1.R;
S = switchingFunction2Codim1.S;

save_vector = S;

file_name = [fld, '/', 'data/surface_vectors.dat'];

dlmwrite(file_name,save_vector,';');

% plot([0,S(1,1)], [0,S(1,2)])
% hold on
% plot([0,S(2,1)], [0,S(2,2)])
% hold off

end

