classdef (Abstract) CircuitPhysical < Circuit
    %Circuit Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        zx_ref
    end
    
    methods
        function [circ] = CircuitPhysical(dynamicsFunction, switchingFunction, ...
                n_surfaces, ...
                n_main, n_alternative, ...
                zx_ref)
            %Circuit Construct an instance of this class
            %   Detailed explanation goes here
            circ@Circuit(dynamicsFunction, switchingFunction, ...
                n_surfaces, ...
                n_main, n_alternative);
            circ.zx_ref = zx_ref;
        end
        
        function [f, M] = flow(circ, w)
            %flow Summary of this method goes here
            %   Detailed explanation goes here
            M = eye(3,3);
            fds = circ.dynamicsFunction;
            u = circ.switchingFunction.vector_field(w);
            
            f = @der;
            
            function [D_t_zx] = der(t, zx) % t, zx
                D_t_zx = fds.f_der(t, zx, u);
            end
        end
        
        function [v] = potential_function(circ, t, zx)
            h = circ.switchingFunction.switching_function(t,zx);
            v = circ.switchingFunction.potential_function(t,h);
        end
        
        function [D_t_V] = potential_derivative(circ, t, zx, u)
            h = circ.switchingFunction.switching_function(t, zx);
            D_t_V = circ.switchingFunction.potential_derivative(t, zx, h, u);
        end
        
        function [x] = nominal_state(circ)
            x = circ.zx_ref;
        end
    end
end

