function projected_eliptical_switching_surface(filename)
%projected_eliptical_switching_surface Summary of this function goes here
%   Detailed explanation goes here
import_analysis

dynamicsGenerator = getDynamicsGenerator();
N = 100;

SZ = dynamicsGenerator.projected_eliptical_hysteresis_surface(N);

dlmwrite(filename, transpose(SZ), '\t');

path(pathdef);
end

