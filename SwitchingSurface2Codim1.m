classdef SwitchingSurface2Codim1 < SwitchingSurface
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [df] = SwitchingSurface2Codim1(switchingFunction2Codim1)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            df@SwitchingSurface(switchingFunction2Codim1);
        end
        
        function [flag] = manifold_point_admissible(fds, idx, zx) % fds, idx, zx
            h = fds.switching_function(zx);
            sf = fds.switchingFunction;
            S = sf.S*h;
            
            if S(idx) > 0
                s = 0;
            else
                s = 1;
            end
            U = fds.control_inputs();
            set_filter = U(idx, :) == s;
            U_filtered = U(:,set_filter);
            
            K = size(U_filtered, 2);
            vector_field_admissible = true;
            k = 1;
            while k <= K && vector_field_admissible
                sv = U_filtered(:,k);
                u = sf.vector_field(sv);
                DtS = sf.S*sf.J_H*sf.f_der([], zx, u);
                vector_field_admissible = S(idx)*DtS(idx) < 0;
                k = k + 1;
            end
            
            flag = vector_field_admissible;
        end
        
        function [SZX] = segment_plotable_hysteresis_manifolds_projected_space(fds, N, epsilon)
            % N is the number or points used for the upper left corner of
            % the parallelogam approximately.
            % Using convention: +ve => <-, -ve => ->
            S = fds.switchingFunction.S;
            
            epsilon = SwitchingSurface2Codim1.scale(epsilon);
            
            epsilon_projection = epsilon;
            r = norm(epsilon_projection, 2);
            ratios = abs(epsilon_projection)./r;
            N_projection = ceil(ratios.*N);
            
            de_1 = epsilon(1)/N_projection(1);
            E_1 = -epsilon(1):de_1:epsilon(1);
            de_2 = epsilon(2)/N_projection(2);
            E_2 = -epsilon(2):de_2:epsilon(2);
            
            m = length(E_1);
            x = [-E_1(1:m); ones(1,m).*epsilon(2)];
            ZX_0 = S\x;
            
            m = length(E_2);
            x = [ones(1,m).*epsilon(1); -E_2(1:m)];
            ZX_1 = S\x;
            
            % Exploit symmetry (quadrants numbered from 0)
            SZX{1,1} = ZX_0;
            SZX{2,1} = -ZX_0;
            SZX{1,2} = ZX_1;
            SZX{2,2} = -ZX_1;
        end
        
        function [SZX] = plotable_hysteresis_manifolds_projected_space(fds, N, epsilon)
            % N is the number or points used for the upper left corner of
            % the parallelogam approximately.
            S = fds.switchingFunction.S;
            
            epsilon = SwitchingSurface2Codim1.scale(epsilon);
            
            epsilon_projection = epsilon;
            r = norm(epsilon_projection, 2);
            ratios = abs(epsilon_projection)./r;
            N_projection = ceil(ratios.*N);
            
            de_1 = epsilon(1)/N_projection(1);
            E_1 = epsilon(1):-de_1:-epsilon(1);
            de_2 = epsilon(2)/N_projection(2);
            E_2 = epsilon(2):-de_2:-epsilon(2);
            
            m = length(E_1)-1;
            x = [E_1(1:m); ones(1,m).*epsilon(2)];
            ZX_0 = S\x;
            
            m = length(E_2)-1;
            x = [-ones(1,m).*epsilon(1); E_2(1:m)];
            ZX_1 = S\x;
            
            % Exploit symmetry (quadrants numbered from 0)
            ZX_2 = -ZX_0;
            ZX_3 = -ZX_1;
            SZX = [ZX_0, ZX_1, ZX_2, ZX_3, ZX_0(:,1)]; % End with index 1 to complete cycle!
        end
        
        function [SZX] = hysteresis_manifolds_projected_space(fds, N, epsilon)
            % N is the number or points used for the upper left corner of
            % the parallelogam approximately.
            % Using convention: +ve => <-, -ve => ->
            S = fds.switchingFunction.S;
            
            epsilon = SwitchingSurface2Codim1.scale(epsilon);
            
            epsilon_projection = epsilon;
            r = norm(epsilon_projection, 2);
            ratios = abs(epsilon_projection)./r;
            N_projection = ceil(ratios.*N);
            
            de_1 = epsilon(1)/N_projection(1);
            E_1 = -epsilon(1):de_1:epsilon(1);
            de_2 = epsilon(2)/N_projection(2);
            E_2 = -epsilon(2):de_2:epsilon(2);
            
            m = length(E_1);
            x = [-E_1(1:m); ones(1,m).*epsilon(2)];
            ZX_0 = S\x;
            
            m = length(E_2);
            x = [ones(1,m).*epsilon(1); -E_2(1:m)];
            ZX_1 = S\x;
            
            % Exploit symmetry (quadrants numbered from 0)
            SZX{1} = [ZX_0, -ZX_0];
            SZX{2} = [ZX_1, -ZX_1];
        end
    end
    
    methods (Static)
        function [epsilon] = scale(epsilon_sq)
            epsilon = epsilon_sq.^(1/2);
        end
    end
end

