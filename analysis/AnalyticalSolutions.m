classdef AnalyticalSolutions
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        dynamicsFunction
        switchingFunction
        epsilon
        
        solver_options
        optimization_options
        non_linear_optimization_options
        
        % Control vectors
        U
        
        % Parameters
        k
        E
        n_turns
        L_m
        C
        i_L_s
        i_L_m_s
        v_C_s
        
        % Transformation parameters
        alpha
    end
    
    methods
        function [anSol] = AnalyticalSolutions(dynamicsFunction, ...
                switchingFunction, epsilon, ...
                solver_tolerance)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            anSol.dynamicsFunction = dynamicsFunction;
            anSol.switchingFunction = switchingFunction;
            anSol.epsilon = epsilon;
            anSol.solver_options = optimoptions('fsolve', 'Algorithm', 'levenberg-marquardt', 'Display', 'off', 'FunctionTolerance', solver_tolerance);
            anSol.optimization_options = optimoptions('linprog','Algorithm','dual-simplex','Display','off');
            % The first crashes matlab 2018a. FFS!
            %anSol.optimization_options = optimoptions('linprog','Algorithm','dual-simplex','Display','off','MaxIterations',1000,'OptimalityTolerance',1e-9,'ConstraintTolerance',1e-9);
            %anSol.optimization_options = optimoptions('linprog','Algorithm','interior-point','Display','off','MaxIterations',1000,'OptimalityTolerance',1e-9,'ConstraintTolerance',1e-9);
            anSol.non_linear_optimization_options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'Display', 'off');
            
            anSol.U = [[0;0],[1;0],[0;1],[1;1]];
            
            anSol.k = anSol.dynamicsFunction.k;
            anSol.E = anSol.dynamicsFunction.E;
            anSol.n_turns = anSol.dynamicsFunction.n_turns;
            anSol.L_m = anSol.dynamicsFunction.L_mag;
            anSol.C = anSol.dynamicsFunction.C;
            anSol.i_L_s = anSol.dynamicsFunction.zx_ref(1);
            anSol.i_L_m_s = anSol.dynamicsFunction.zx_ref(2);
            anSol.v_C_s = anSol.dynamicsFunction.zx_ref(3);
            
            anSol.alpha = [0,1,0]*(dynamicsFunction.physical_space([0;0;1]) - dynamicsFunction.zx_ref);
        end
        
        function [dz] = f_der_sld(anSol, zz_3, u)
            dynf = anSol.dynamicsFunction;
            zx = dynf.physical_space([0;0;zz_3]);
            dz = dynf.T_trs*dynf.f_der([], zx, u);
        end
        
        function [dx, V, Delta] = vector_fields(anSol, zx)
            %vector_fields Summary of this method goes here
            %   Detailed explanation goes here
            dynf = anSol.dynamicsFunction;
            
            dx = zeros(3,4);
            dx(:,1) = dynf.f_der([], zx, anSol.U(:,1));
            dx(:,2) = dynf.f_der([], zx, anSol.U(:,2));
            dx(:,3) = dynf.f_der([], zx, anSol.U(:,3));
            dx(:,4) = dynf.f_der([], zx, anSol.U(:,4));
            
            Delta = [dx(:,1) - dx(:,2), dx(:,1) - dx(:,3), dx(:,1) - dx(:,4)];
            
            V = (1/2)*det(Delta);
        end
        
        function [u, m, D] = duty_ratio2ctrl(anSol, gamma)
            u = anSol.U*gamma;
            m = u(1,:) - u(2,:);
            D = u(1,:).*u(2,:);
        end
        
        function [gamma] = ctrl2duty_ratio_independent(anSol, u)
            N = size(anSol.U,2);
            gamma = zeros(N,size(u,2));
            for n = 1:N
                gamma(n,:) = select(anSol.U(:,n));
            end
            
            function [v] = select(idx)
                v = (1-idx(1))*(1-idx(2))*(1-u(1,:)).*(1-u(2,:)) + ...
                    idx(1)*(1-idx(2))*u(1,:).*(1-u(2,:)) + ...
                    (1-idx(1))*idx(2)*(1-u(1,:)).*u(2,:) + ...
                    idx(1)*idx(2)*u(1,:).*u(2,:);
            end
        end
        
        function [zz_3_feas, zz_1_2_min, f_sld_min, gamma_min, zz_1_2_max, f_sld_max, gamma_max] = hysteresis_convex_hull_speed(anSol, zz_3, epsilon)
            N = length(zz_3);
            
            zz_3_feas_stack = Stack();
            zz_1_2_min_stack = Stack();
            f_sld_min_stack = Stack();
            gamma_min_stack = Stack();
            zz_1_2_max_stack = Stack();
            f_sld_max_stack = Stack();
            gamma_max_stack = Stack();
            
            x0= [0; 0; 0; 0; 0; 0];
            x0_min = x0;
            x0_max = x0;
            A = []; b = []; Aeq = []; beq = [];
            ub = [Inf;Inf; 1; 1; 1; 1];
            lb = [-Inf;-Inf; 0; 0; 0; 0];
            for n = 1:N
                zz_3_idx = zz_3(n);
                nonlcon = get_con(zz_3_idx);
                fun = get_f(zz_3_idx);
                [x,fval,exitflag,~,~,~,~] = fmincon(fun,x0_min,A,b,Aeq,beq,lb,ub,nonlcon ,...
                    anSol.non_linear_optimization_options); % x,fval,exitflag,output,lambda,grad,hessian
                solution_found = exitflag == 1;
                if solution_found
                    zz_1_2_min_stack = zz_1_2_min_stack.push(x(1:2));
                    x0_min = x;
                    gamma_min_stack = gamma_min_stack.push(x(3:6));
                    f_sld_min_stack = f_sld_min_stack.push(fval);
                    
                    f = @(x)(-fun(x));
                    [x,fval,~,~,~,~,~] = fmincon(f,x0_max,A,b,Aeq,beq,lb,ub,nonlcon, ...
                        anSol.non_linear_optimization_options);
                    zz_1_2_max_stack = zz_1_2_max_stack.push(x(1:2));
                    x0_max = x;
                    gamma_max_stack = gamma_max_stack.push(x(3:6));
                    fval = -fval;
                    f_sld_max_stack = f_sld_max_stack.push(fval);
                    
                    zz_3_feas_stack = zz_3_feas_stack.push(zz_3_idx);
                end
            end
            
            zz_3_feas = cell2mat(zz_3_feas_stack.toCell());
            zz_1_2_min = cell2mat(zz_1_2_min_stack.toCell());
            gamma_min = cell2mat(gamma_min_stack.toCell());
            f_sld_min = cell2mat(f_sld_min_stack.toCell());
            zz_1_2_max = cell2mat(zz_1_2_max_stack.toCell());
            gamma_max = cell2mat(gamma_max_stack.toCell());
            f_sld_max = cell2mat(f_sld_max_stack.toCell());
            
            function [f] = get_f(zz_3)
                sf = anSol.switchingFunction;
                f = @fcn;
                
                function [fy] = fcn(y)
                    zz = [y(1:2); zz_3];
                    gamma = y(3:6);
                    zx = sf.physical_space(zz);
                    
                    [F, ~, ~] = anSol.vector_fields(zx);
                    Fx = F*gamma;
                    fy = Fx(3);
                end
            end
            
            function [con] = get_con(zz_3)
                sf = anSol.switchingFunction;
                con = @fcn;
                
                function [c, ceq] = fcn(y)
                    zz = [y(1:2); zz_3];
                    gamma = y(3:6);
                    zx = sf.physical_space(zz);
                    
                    [F, ~, ~] = anSol.vector_fields(zx);
                    Fx = F*gamma;
                    simplex = ones(1,4)*gamma - 1;
                    ceq = [Fx(1:2); simplex];
                    
                    h = sf.switching_function([], zx);
                    v = sf.potential_function([], h);
                    c = v - epsilon;
                end
            end
        end 
        
        function [zz_3_feas, f_sld_min, gamma_min, f_sld_max, gamma_max] = conventional_convex_hull_speed(anSol, zz_3)
            dynf = anSol.dynamicsFunction;
            N = length(zz_3);
            
            zz_3_feas_stack = Stack();
            f_sld_min_stack = Stack();
            gamma_min_stack = Stack();
            f_sld_max_stack = Stack();
            gamma_max_stack = Stack();
            
            for n = 1:N
                zz_3_idx = zz_3(n);
                zx  = dynf.physical_space([0; 0; zz_3_idx]);
                [dx, ~, ~] = anSol.vector_fields(zx);
                
                T_proj = anSol.switchingFunction.T_trs;
                proj_3 = T_proj(3,:);
                proj_1_2 = T_proj(1:2,:);
                
                f = transpose(proj_3*dx);
                A = [-eye(4,4);eye(4,4)];
                b = [zeros(4,1);ones(4,1)];
                Aeq = [proj_1_2*dx; ones(1,4)];
                beq = [0;0;1];
                
                [x, fval, exitflag, ~, ~] = linprog(f, A, b, Aeq, beq, [], [], anSol.optimization_options); % x,fval,exitflag,output,lambda
                no_feasible_point_was_found = exitflag == -2;
                problem_infeasible = exitflag == -5;
                if ~(no_feasible_point_was_found || problem_infeasible)
                    gamma_min_stack = gamma_min_stack.push(x);
                    f_sld_min_stack = f_sld_min_stack.push(fval);
                    
                    f = -f;
                    [x, fval, ~, ~, ~] = linprog(f, A, b, Aeq, beq, [], [], anSol.optimization_options); % x,fval,exitflag,output,lambda
                    gamma_max_stack = gamma_max_stack.push(x);
                    fval = -fval;
                    f_sld_max_stack = f_sld_max_stack.push(fval);
                    
                    zz_3_feas_stack = zz_3_feas_stack.push(zz_3_idx);
                end
            end
            
            zz_3_feas = cell2mat(zz_3_feas_stack.toCell());
            gamma_min = cell2mat(gamma_min_stack.toCell());
            f_sld_min = cell2mat(f_sld_min_stack.toCell());
            gamma_max = cell2mat(gamma_max_stack.toCell());
            f_sld_max = cell2mat(f_sld_max_stack.toCell());
        end
        
        function [zz_3_feas, D_t_zz_3, u, m, D] = canopy_speed(anSol, zz_3)
            N = length(zz_3);
            u = zeros(2,N);
            m = zeros(1,N);
            D = zeros(1,N);
            
            for n = 1:N
                [u(:,n), m(n), D(n)] = anSol.canopy_solution(zz_3(n));
            end
            
            flt_0 = u(1,:) > 0 & u(2,:) > 0;
            flt_1 = u(1,:) < 1 & u(2,:) < 1;
            flt = flt_0 & flt_1;
            
            zz_3_feas = zz_3(flt);
            u = u(:,flt);
            m = m(flt);
            D = D(flt);
            
            N = length(zz_3_feas);
            D_t_zz_3 = zeros(1,N);
            for n = 1:N
                D_t_zz = anSol.f_der_sld(zz_3_feas(n), u(:,n));
                D_t_zz_3(n) = D_t_zz(3);
            end
        end
        
        function [zz_3_feas, D_t_zz_3, u, m, D] = canopy_speed_non_ideal(anSol, zz_3)
            u_stack = Stack();
            m_stack = Stack();
            D_stack = Stack();
            D_t_zz_3_stack = Stack();
            zz_3_flt_stack = Stack();
            
            N = length(zz_3);
            for n = 1:N
                [u, m, D, solution_exists] = anSol.canopy_solution_non_ideal(zz_3(n));
                if solution_exists
                    lb_constraint = u(1) > 0 & u(2) > 0;
                    ub_constraint = u(1) < 1 & u(2) < 1;
                    admissible = lb_constraint && ub_constraint;
                    if admissible
                        zz_3_flt_stack = zz_3_flt_stack.push(zz_3(n));
                        
                        u_stack = u_stack.push(u);
                        m_stack = m_stack.push(m);
                        D_stack = D_stack.push(D);
                        
                        D_t_zz = anSol.f_der_sld(zz_3(n), u);
                        D_t_zz_3_stack = D_t_zz_3_stack.push(D_t_zz(3));
                    end
                end
            end
            
            zz_3_feas = cell2mat(zz_3_flt_stack.toCell());
            D_t_zz_3 = cell2mat(D_t_zz_3_stack.toCell());
            u = cell2mat(u_stack.toCell());
            m = cell2mat(m_stack.toCell());
            D = cell2mat(D_stack.toCell());
        end
        
        function [u, m, D] = canopy_solution(anSol, zz_3)
            %sliding_speed Summary of this function goes here
            %   Detailed explanation goes here
            
            i_L_m = anSol.alpha*zz_3 + anSol.i_L_m_s;
            v_C_sliding = anSol.v_C_s - anSol.k*(i_L_m - anSol.i_L_m_s);
            
            %m = anSol.E(2)/(v_C_sliding + (1/anSol.n_turns)*(v_C_sliding - anSol.E(1)));
            m = anSol.n_turns*anSol.E(2)/((anSol.n_turns + 1)*v_C_sliding - anSol.E(1));
            %D = ((k/L_m)*(v_C_sliding - E(1)) - i_L_m/C + (m*(n+1)/C)*i_L_s) / ((k/L_m)*((n+1)*v_C_sliding - E(1)) - ((n+1)/C)*i_L_m);
            D = ( (anSol.k/anSol.L_m)*(v_C_sliding - anSol.E(1)) + (1/anSol.C)*(m*(anSol.n_turns+1)*anSol.i_L_s - i_L_m) ) ...
                / ( (anSol.k/anSol.L_m)*((anSol.n_turns+1)*v_C_sliding - anSol.E(1)) - (1/anSol.C)*(anSol.n_turns+1)*i_L_m );
            
            %Di_L_m = (1/L_m)*(((1-D)/n)*(E(1) - v_C_sliding) + D*v_C_sliding);
            
            Delta_sr = (m^2 + 4*D)^(1/2);
            u_p = (1/2)*(m + Delta_sr);
            u_m = (1/2)*(-m + Delta_sr);
            
            u = [u_p; u_m];
        end
        
        function [u, m, D, solution_exists] = canopy_solution_non_ideal(anSol, zz_3)
            dynf = anSol.dynamicsFunction;
            
            [u_0, ~, ~] = anSol.canopy_solution(zz_3);
            
            zx = dynf.physical_space([0;0;zz_3]);
            f = get_dynamics(zx);
            
            [u, ~, exitflag, ~, ~] = fsolve(f, u_0, anSol.solver_options); % x,fval,exitflag,output,jacobian
            solution_exists = ~(exitflag <=0);
            if solution_exists
                D = u(1)*u(2);
                m = u(1) - u(2);
            else
                D = [];
                m = [];
                u = [];
            end
            
            function [f] = get_dynamics(x)
                f = @dyn;
                function [dx] = dyn(u)
                    dx = dynf.J_H*dynf.f_der([], x, u);
                end
            end
        end
        
        function [del_zz_3_D_t_zz_3] = canopy_gradiant(anSol, zz_3)
            N = length(zz_3);
            del_zz_3_D_t_zz_3 = zeros(N,1);
            for n = 1:N
                i_L_m = anSol.alpha*zz_3(n) + anSol.i_L_m_s;
                v_C_sliding = anSol.v_C_s - anSol.k*(i_L_m - anSol.i_L_m_s);
                
                m = anSol.n_turns*anSol.E(2)/((anSol.n_turns + 1)*v_C_sliding - anSol.E(1));
                D = ( (anSol.k/anSol.L_m)*(v_C_sliding - anSol.E(1)) + (1/anSol.C)*(m*(anSol.n_turns+1)*anSol.i_L_s - i_L_m) ) ...
                    / ( (anSol.k/anSol.L_m)*((anSol.n_turns+1)*v_C_sliding - anSol.E(1)) - (1/anSol.C)*(anSol.n_turns+1)*i_L_m );
                
                del_zz_3_m = ( (anSol.n_turns + 1)*anSol.k*anSol.alpha*anSol.E(2) ) / ((anSol.n_turns + 1)*v_C_sliding - anSol.E(1))^2;
                
                denom = (anSol.k/anSol.L_m)*((anSol.n_turns + 1)*v_C_sliding - anSol.E(1)) + (1/anSol.C)*(anSol.n_turns + 1)*i_L_m;
                del_zz_3_D = (- (anSol.k^2*anSol.alpha)/anSol.L_m + (1/anSol.C)*((anSol.n_turns + 1)*anSol.i_L_s*del_zz_3_m - anSol.alpha)) / denom ...
                    - (-anSol.k^2*(anSol.n_turns + 1)*anSol.alpha/anSol.L_m + (1/anSol.C)*(anSol.n_turns + 1)*anSol.alpha) * ((anSol.k/anSol.L_m)*(v_C_sliding - anSol.E(1)) + (1/anSol.C)*(m*(anSol.n_turns+1)*anSol.i_L_s - i_L_m)) / (denom^2);
                
                del_zz_3_D_t_zz_3(n) = (1/(anSol.alpha*anSol.n_turns*anSol.L_m)) * (del_zz_3_D*((anSol.n_turns + 1)*v_C_sliding - anSol.E(1)) - D*(anSol.n_turns+1)*anSol.k*anSol.alpha + anSol.k*anSol.alpha);
            end
        end
        
        function [del_zz_3_D_t_zz_3] = canopy_gradiant_parametric_analysis(anSol, zz_3, kappa)
            N = length(kappa);
            del_zz_3_D_t_zz_3 = zeros(N,1);
            for n = 1:N
                alpha_k = 1/(1 + kappa(n)^2)^(1/2);
                i_L_m = - alpha_k*zz_3 + anSol.i_L_m_s;
                v_C_sliding = anSol.v_C_s + kappa(n)*alpha_k*zz_3;
                
                m = anSol.n_turns*anSol.E(2)/((anSol.n_turns + 1)*v_C_sliding - anSol.E(1));
                D = ( (kappa(n)/anSol.L_m)*(v_C_sliding - anSol.E(1)) + (1/anSol.C)*(m*(anSol.n_turns+1)*anSol.i_L_s - i_L_m) ) ...
                    / ( (kappa(n)/anSol.L_m)*((anSol.n_turns+1)*v_C_sliding - anSol.E(1)) - (1/anSol.C)*(anSol.n_turns+1)*i_L_m );
                
                del_zz_3_m = ( (anSol.n_turns + 1)*kappa(n)*alpha_k*anSol.E(2) ) / ((anSol.n_turns + 1)*v_C_sliding - anSol.E(1))^2;
                
                denom = (kappa(n)/anSol.L_m)*((anSol.n_turns + 1)*v_C_sliding - anSol.E(1)) + (1/anSol.C)*(anSol.n_turns + 1)*i_L_m;
                del_zz_3_D = ( (kappa(n)^2*alpha_k)/anSol.L_m + (1/anSol.C)*((anSol.n_turns + 1)*anSol.i_L_s*del_zz_3_m - alpha_k)) / denom ...
                    - (kappa(n)^2*(anSol.n_turns + 1)*alpha_k/anSol.L_m + (1/anSol.C)*(anSol.n_turns + 1)*alpha_k) * ((kappa(n)/anSol.L_m)*(v_C_sliding - anSol.E(1)) + (1/anSol.C)*(m*(anSol.n_turns+1)*anSol.i_L_s - i_L_m)) / (denom^2);
                
                del_zz_3_D_t_zz_3(n) = -(1/(alpha_k*anSol.n_turns*anSol.L_m)) * (del_zz_3_D*((anSol.n_turns + 1)*v_C_sliding - anSol.E(1)) - D*(anSol.n_turns+1)*kappa(n)*alpha_k + kappa(n)*alpha_k);
            end
        end
        
        function [kappa_root] = canopy_gradiant_denominator_root(anSol)
            zz_3 = 0;
            kappa_0 = 32;
            
            f = @denominator;
            [kappa_root, ~, ~, ~, ~] = fsolve(f, kappa_0, anSol.solver_options); % x,fval,exitflag,output,jacobian
            
            function [d] = denominator(k)
                d = anSol.canopy_gradiant_denominator(zz_3, k);
            end
        end
        
        function [denom] = canopy_gradiant_denominator(anSol, zz_3, kappa)
            i_L_m = anSol.alpha*zz_3 + anSol.i_L_m_s;
            v_C_sliding = anSol.v_C_s - kappa*anSol.alpha*zz_3;
            
            denom = (kappa/anSol.L_m)*((anSol.n_turns + 1)*v_C_sliding - anSol.E(1)) + (1/anSol.C)*(anSol.n_turns + 1)*i_L_m;
        end
        
        function [u, m, D] = canopy_solution_from_magnetizing_current(anSol, i_L_m)
            %sliding_speed Summary of this function goes here
            %   Detailed explanation goes here
            
            v_C_sliding = anSol.v_C_s - anSol.k*(i_L_m - anSol.i_L_m_s);
            
            m = anSol.E(2)/(v_C_sliding + (1/anSol.n_turns)*(v_C_sliding - anSol.E(1)));
            %D = ((k/L_m)*(v_C_sliding - E(1)) - i_L_m/C + (m*(n+1)/C)*i_L_s) / ((k/L_m)*((n+1)*v_C_sliding - E(1)) - ((n+1)/C)*i_L_m);
            D = ( (anSol.k/anSol.L_m)*(v_C_sliding - anSol.E(1)) + (1/anSol.C)*(m*(anSol.n_turns+1)*anSol.i_L_s - i_L_m) ) ...
                / ( (anSol.k/anSol.L_m)*((anSol.n_turns+1)*v_C_sliding - anSol.E(1)) - (1/anSol.C)*(anSol.n_turns+1)*i_L_m );
            
            %Di_L_m = (1/L_m)*(((1-D)/n)*(E(1) - v_C_sliding) + D*v_C_sliding);
            
            Delta_sr = (m^2 + 4*D)^(1/2);
            u_p = (1/2)*(m + Delta_sr);
            u_m = (1/2)*(-m + Delta_sr);
            
            u = [u_p; u_m];
        end
    end
end

