function [n_init, n_fin] = detect_cycle(x, n_min_length)
%detect_convergence A function for numerically detecting a cycle in a
%trajectory
%   The input to the function is the trajectory and the minimal allowed
%   length for the cycle. Output are the initial and final entries for the
%   cycle in the trajectory.

N = size(x,2);
N_start_max = N - n_min_length;

minimal_lengths = cell(N_start_max,1);

for n_start = 1:N_start_max
    n_cycle_end_min = n_start + n_min_length;
    
    x_init = x(:,n_start);
    for n = n_cycle_end_min:N
        x_fin = x(:,n);
        d = norm(x_init - x_fin);
        if isempty(minimal_lengths{n_start})
            minimal_lengths{n_start}.distance = d;
            minimal_lengths{n_start}.n_fin = n;
        else
            if minimal_lengths{n_start}.distance >= d
                minimal_lengths{n_start}.distance = d;
                minimal_lengths{n_start}.n_fin = n;
            end
        end
    end
end

minimal_length_array = zeros(N_start_max,1);
for n = 1:N_start_max
    minimal_length_array(n) = minimal_lengths{n}.distance;
end
[~, n_min] = min(minimal_length_array);

n_init = n_min;
n_fin = minimal_lengths{n_min}.n_fin;

end

