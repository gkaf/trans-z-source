classdef SRCDetectionDiscreteStateEnumeration < DiscreteStateEnumeration
    %FilterDiscreteStateEnumeration Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/FilterDiscreteStateEnumeration.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    enumeration
        % Convergence detection
        StartConvergenceDetection
        ReachStableState
        
        ResetTime
        
        EvaluateSwtchingRatios
        DetectSwitchingRatiosConvergence
        
        CheckConvergence
    end
    
    methods
        function [fdse] = SRCDetectionDiscreteStateEnumeration()
            fdse@DiscreteStateEnumeration();
        end
    end
    
end

