classdef ResetTime < OutputFunctions & DiscreteStateJump
    %ResetTimeToEvalulate Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/Initial.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods
        function [ds] = ResetTime(discreteStateData, u, circuit, continuum_output_size)
            %CheckSRConvergence Construct an instance of this class
            %   Detailed explanation goes here
            
            ds@OutputFunctions(circuit);
            ds@DiscreteStateJump(SRCDetectionDiscreteStateEnumeration.ResetTime, discreteStateData, u, continuum_output_size);
            %flowIntegrator, interval, ...
        end
        
        function [e_idx] = guard(ds, ~, ~) % ds, t, x
            if isa(ds.discreteStateData, 'EvaluatingDutyRatios')
                e_idx = 1;
            elseif isa(ds.discreteStateData, 'DetectingDutyRatiosConvergence')
                e_idx = 2;
            else
                e_idx = 0;
            end
        end
        
        function [stateTransition, t_post, x_post] = jump(ds, e_idx, t_pre, x_pre) % ds, e_idx, t_pre, x_pre
            discreteStateData = ds.discreteStateData;
            u = ds.u;
            if e_idx == 1
                discreteStateEnumeration = SRCDetectionDiscreteStateEnumeration.EvaluateSwtchingRatios;
                stateTransition = StateId(discreteStateEnumeration, discreteStateData, u);
            elseif e_idx == 2
                discreteStateEnumeration = SRCDetectionDiscreteStateEnumeration.DetectSwitchingRatiosConvergence;
                stateTransition = StateId(discreteStateEnumeration, discreteStateData, u);
            else
                stateTransition = {};
            end
            
            t_post = t_pre;
            x_post = ds.reset_starting_state(x_pre, t_pre);
        end
    end
    
    methods
        function [x] = reset_starting_state(ds, x, t_init)
            circuit = ds.circuit;
            t_init_idx = circuit.n_main + 1;
            x(t_init_idx) = t_init;
            
            zz_sliding_idx = circuit.n_main;
            x(zz_sliding_idx) = 0;
        end
    end
    
end

