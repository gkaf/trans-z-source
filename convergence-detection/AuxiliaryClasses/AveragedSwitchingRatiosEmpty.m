classdef AveragedSwitchingRatiosEmpty < AveragedSwitchingRatios
    %InitializeConvergenceCheck Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [icc] = AveragedSwitchingRatiosEmpty(t, zx, u)
            %InitializeConvergenceCheck Construct an instance of this class
            %   Detailed explanation goes here
            checkPeriod = CheckPeriod(t, zx, u, t, zx, u);
            icc@AveragedSwitchingRatios(checkPeriod);
        end
        
        function [icc] = update(icc, t, zx, u)
            if t > icc.checkPeriod.t_fin
                icc = AveragedSwitchingRatiosInitialized(icc.checkPeriod, t, zx, u);
            else
                msgID = 'AveragedSwitchingRatiosEmpty:Exception';
                msgtext = 'Simulation time did not progress, averages undefined due to division by zero.';
                ME = MException(msgID, msgtext);
                throw(ME);
            end
        end
    end
end

