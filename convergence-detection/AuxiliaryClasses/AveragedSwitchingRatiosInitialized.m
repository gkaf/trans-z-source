classdef AveragedSwitchingRatiosInitialized <  AveragedSwitchingRatios
    %InitializeConvergenceCheck Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        gamma_int
    end
    
    methods
        function [icc] = AveragedSwitchingRatiosInitialized(checkPeriod, t_fin, zx_fin, u_fin)
            %InitializeConvergenceCheck Construct an instance of this class
            %   Detailed explanation goes here
            icc@AveragedSwitchingRatios(checkPeriod);
            
            u = icc.checkPeriod.u_fin;
            t = icc.checkPeriod.t_fin;
            gamma = AveragedSwitchingRatiosInitialized.get_gamma_planar(u);
            
            icc.checkPeriod = icc.checkPeriod.extendEnd(t_fin, zx_fin, u_fin);
            
            Dt = t_fin - t;
            icc.gamma_int = Dt.*gamma;
        end
        
        function [icc] = update(icc, t_fin, zx_fin, u_fin)
            u = icc.checkPeriod.u_fin;
            t = icc.checkPeriod.t_fin;
            gamma = AveragedSwitchingRatiosInitialized.get_gamma_planar(u);
            
            icc.checkPeriod = icc.checkPeriod.extendEnd(t_fin, zx_fin, u_fin);
            
            Dt = t_fin - t;
            icc.gamma_int = icc.gamma_int + Dt.*gamma;
        end
        
        function [gamma_mean] = getMeanDiscreteState(icc)
            Dt = icc.checkPeriod.Dt;
            gamma_mean = icc.gamma_int ./ Dt;
        end
        
        function [D_t_mean_zz_3] = getMeanSlidingSpeed(icc, checkConvergence)
            zx_fin = icc.checkPeriod.zx_fin;
            Delta_zz_3 = checkConvergence.slide(zx_fin);
            Dt = icc.checkPeriod.Dt;
            D_t_mean_zz_3 = Delta_zz_3 ./ Dt;
        end
    end
    
    methods (Static)
        function [gamma] = get_gamma_planar(u)
            gamma = [(1-u(1))*(1-u(2)), (1-u(1))*u(2); ...
                u(1)*(1-u(2)), u(1)*u(2)];
        end
    end
end

