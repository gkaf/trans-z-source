classdef (Abstract) AveragedSwitchingRatios
    %InitializeConvergenceCheck Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        checkPeriod
    end
    
    methods
        function [icc] = AveragedSwitchingRatios(checkPeriod)
            %InitializeConvergenceCheck Construct an instance of this class
            %   Detailed explanation goes here
            icc.checkPeriod = checkPeriod;
        end
        
        function [t, zx, u] = final_point(icc)
            t = icc.checkPeriod.t_fin;
            zx = icc.checkPeriod.zx_fin;
            u = icc.checkPeriod.u_fin;
        end
        
        function [checkPeriod] = getCheckPeriod(icc)
            checkPeriod = icc.checkPeriod;
        end
    end
    
    methods (Abstract)
        [icc] = update(icc, t, zx, u);
    end
end

