classdef CheckPeriod
    %CheckPoint Contains the information required to start the simulation
    %from a given starting point.
    %   Detailed explanation goes here
    
    properties
        t_init
        zx_init
        u_init
        t_fin
        zx_fin
        u_fin
        Dt
    end
    
    methods
        function [cp] = CheckPeriod(t_init, zx_init, u_init, ...
                t_fin, zx_fin, u_fin)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            cp.t_init = t_init;
            cp.zx_init = zx_init;
            cp.u_init = u_init;
            cp.t_fin = t_fin;
            cp.zx_fin = zx_fin;
            cp.u_fin = u_fin;
            cp.Dt = t_fin - t_init;
        end
        
        function [cp] = extendEnd(cp, t_fin, zx_fin, u_fin)
            cp.t_fin = t_fin;
            cp.zx_fin = zx_fin;
            cp.u_fin = u_fin;
            cp.Dt = cp.t_fin - cp.t_init;
        end
        
        function [cp] = combine(cp1, cp2)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            cp = cp1;
            cp.t_fin = cp2.t_fin;
            cp.zx_fin = cp2.zx_fin;
            cp.u_fin = cp2.u_fin;
            cp.Dt = cp.t_fin - cp.t_init;
        end
    end
end

