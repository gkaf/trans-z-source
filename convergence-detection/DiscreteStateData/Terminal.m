classdef Terminal < DiscreteStateData
    %Terminal Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        problem_description
    end
    
    methods
        function [t] = Terminal(problem_description)
            %UNTITLED3 Construct an instance of this class
            %   Detailed explanation goes here
            t@DiscreteStateData();
            t.problem_description = problem_description;
        end
    end
end

 