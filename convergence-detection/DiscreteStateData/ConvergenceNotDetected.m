classdef ConvergenceNotDetected < Terminal
    %ConvergenceNotDetected Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [cnd] = ConvergenceNotDetected()
            %ConvergenceNotDetected Construct an instance of this class
            %   Detailed explanation goes here
            cnd@Terminal("Duty ratios did not converge.");
        end
    end
end

