classdef BurnInState < DiscreteStateData
    %BurnInState The state holds the minimum ammount of time the system
    %must be run in order to stabilize.
    %   The switching ratios of the system are proven to converge when the
    %   system reaches its steady state, whether the steady state motion is
    %   periodic or chaotic. The burn in state holds an amoun of time so
    %   that after the system was run for at least this amoun of time the
    %   swithcing duty ratios have stabilized.
    
    properties
        checkPeriod
    end
    
    methods
        function [bis] = BurnInState(t_init, zx_init, u_init)
            %BurnInState Note the required burn in time and the simulation
            %starting time.
            %   Detailed explanation goes here
            bis@DiscreteStateData();
            bis.checkPeriod = CheckPeriod(t_init, zx_init, u_init, t_init, zx_init, u_init);
        end
        
        function [bis] = update(bis, t_fin, zx_fin, u_fin)
            bis.checkPeriod = bis.checkPeriod.extendEnd(t_fin, zx_fin, u_fin);
        end
        
        function [evaluatingDutyRatios] = getEvaluatingDutyRatios(bis)
            %burnInComplete Check if the system has been run for the burn
            %in time.
            %   Detailed explanation goes here
            evaluatingDutyRatios = EvaluatingDutyRatios(bis.checkPeriod);
        end
    end
end

