classdef DetectingDutyRatiosConvergence < DiscreteStateData
    %DetectingDutyRatiosConvergence Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        N_transient_extensions_ub
        n_transient_extensions
        Delta_t_period_multiple
        eta
        
        burn_in_checkPeriod
        previous_averagedSwitchingRatios
        current_averagedSwitchingRatios
    end
    
    methods
        function [ddrc] = DetectingDutyRatiosConvergence(burn_in_checkPeriod, previous_averagedSwitchingRatios, ...
                eta, N_transient_extensions_ub, Delta_t_period_multiple)
            %DetectingDutyRatiosConvergence Construct an instance of this class
            %   Detailed explanation goes here
            ddrc@DiscreteStateData();
            
            ddrc.eta = eta;
            ddrc.N_transient_extensions_ub = N_transient_extensions_ub;
            ddrc.Delta_t_period_multiple = Delta_t_period_multiple;
            
            ddrc.burn_in_checkPeriod = burn_in_checkPeriod;
            ddrc.previous_averagedSwitchingRatios = previous_averagedSwitchingRatios;
            
            [t, zx, u] = previous_averagedSwitchingRatios.final_point();
            
            ddrc.current_averagedSwitchingRatios = AveragedSwitchingRatiosEmpty(t, zx, u);
            ddrc.n_transient_extensions = 0;
        end
        
        function [ddrc] = update(ddrc, t_fin, zx_fin, u_fin)
            %update Summary of this method goes here
            %   Detailed explanation goes here
            ddrc.current_averagedSwitchingRatios = ddrc.current_averagedSwitchingRatios.update(t_fin, zx_fin, u_fin);
        end
        
        function [flag] = converged(ddrc)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            prev = ddrc.previous_averagedSwitchingRatios;
            cur = ddrc.current_averagedSwitchingRatios;
            if isa(prev, 'AveragedSwitchingRatiosInitialized') && isa(cur, 'AveragedSwitchingRatiosInitialized')
                u_prev = prev.getMeanDiscreteState();
                u_cur = cur.getMeanDiscreteState();
                flag = norm(u_cur - u_prev, 2) < ddrc.eta;
            else
                flag = false;
            end
        end
        
        function [ddrc] = extend_transient_duration(ddrc)
            checkPeriod = ddrc.previous_averagedSwitchingRatios.getCheckPeriod();
            ddrc.burn_in_checkPeriod = ddrc.burn_in_checkPeriod.combine(checkPeriod);
            
            ddrc.previous_averagedSwitchingRatios = ddrc.current_averagedSwitchingRatios;
            
            [t, zx, u] = ddrc.current_averagedSwitchingRatios.final_point();
            ddrc.previous_averagedSwitchingRatios = ddrc.previous_averagedSwitchingRatios.update(t, zx, u);
            
            [t, zx, u] = ddrc.previous_averagedSwitchingRatios.final_point();
            ddrc.current_averagedSwitchingRatios = AveragedSwitchingRatiosEmpty(t, zx, u);
            
            ddrc.n_transient_extensions = ddrc.n_transient_extensions + 1;
        end
        
        function [ddrc] = extend_cycle_duration(ddrc)
            [t, zx, u] = ddrc.current_averagedSwitchingRatios.final_point();
            ddrc.previous_averagedSwitchingRatios = ddrc.previous_averagedSwitchingRatios.update(t, zx, u);
            
            ddrc.current_averagedSwitchingRatios = AveragedSwitchingRatiosEmpty(t, zx, u);
            
            ddrc.n_transient_extensions = 0;
            ddrc.Delta_t_period_multiple = 2*ddrc.Delta_t_period_multiple;
        end
        
        function [Delta_t_burn_in] = burn_in_period(ddrc)
            Delta_t_burn_in = ddrc.burn_in_checkPeriod.Dt;
        end
        
        function [Delta_t] = period_multiple(ddrc)
            Delta_t = ddrc.Delta_t_period_multiple;
        end
        
        function [Delta_t_total] = total_time(ddrc)
            t_init = ddrc.burn_in_checkPeriod.t_init;
            t_fin = ddrc.current_averagedSwitchingRatios.checkPeriod.t_fin;
            Delta_t_total = t_fin - t_init;
        end
        
        function [gamma_mean] = mean_discrete_state(ddrc)
            gamma_mean = ddrc.previous_averagedSwitchingRatios.getMeanDiscreteState();
        end
        
        function [D_t_zz_3_mean] = mean_sliding_speed(ddrc, checkConvergence)
            D_t_zz_3_mean = ddrc.previous_averagedSwitchingRatios.getMeanSlidingSpeed(checkConvergence);
        end
    end
end

