classdef EvaluatingDutyRatios < DiscreteStateData
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        burn_in_checkPeriod
        averagedSwitchingRatios
    end
    
    methods
        function [bic] = EvaluatingDutyRatios(checkPeriod)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            bic@DiscreteStateData();
            bic.burn_in_checkPeriod = checkPeriod;
            
            t = bic.burn_in_checkPeriod.t_fin;
            zx = bic.burn_in_checkPeriod.zx_fin;
            u = bic.burn_in_checkPeriod.u_fin;
            
            bic.averagedSwitchingRatios = AveragedSwitchingRatiosEmpty(t, zx, u);
        end
        
        function [bic] = update(bic, t_fin, zx_fin, u_fin)
            %update Summary of this method goes here
            %   Detailed explanation goes here
            bic.averagedSwitchingRatios = bic.averagedSwitchingRatios.update(t_fin, zx_fin, u_fin);
        end
        
        function [detectingDutyRatiosConvergence] = getDetectingDutyRatiosConvergence(bic, ...
                eta, N_transient_extensions_ub, Delta_t_period_multiple)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            detectingDutyRatiosConvergence = DetectingDutyRatiosConvergence(bic.burn_in_checkPeriod, bic.averagedSwitchingRatios, ...
                eta, N_transient_extensions_ub, Delta_t_period_multiple);
        end
    end
end

