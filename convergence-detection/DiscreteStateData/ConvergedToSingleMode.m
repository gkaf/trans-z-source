classdef ConvergedToSingleMode < Terminal
    %ConvergedToSingleMode Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [car] = ConvergedToSingleMode()
            %ConvergedToSingleMode Construct an instance of this class
            %   Detailed explanation goes here
            car@Terminal("System converged to an attractor existing in a system mode.");
        end
        
    end
end

