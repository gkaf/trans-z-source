classdef MeanStates < Result
    %MeanStates Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        gamma_mean
        D_t_zz_s_mean
        
        zx_fin
        u_fin
        
        Delta_t_burn_in
        Delta_t_period_multiple
        Delta_t_total
    end
    
    methods
        function [ms] = MeanStates(gamma_mean, D_t_zz_s_mean, ...
                zx_fin, u_fin, ....
                Delta_t_burn_in, Delta_t_period_multiple, Delta_t_total)
            %MeanStates Construct an instance of this class
            %   Detailed explanation goes here
            ms@Result();
            ms.gamma_mean = gamma_mean;
            ms.D_t_zz_s_mean = D_t_zz_s_mean;
            
            ms.zx_fin = zx_fin;
            ms.u_fin = u_fin;
            
            ms.Delta_t_burn_in = Delta_t_burn_in;
            ms.Delta_t_period_multiple = Delta_t_period_multiple;
            ms.Delta_t_total = Delta_t_total;
        end
        
        function [gamma_mean] = mean_discrete_state(ms)
            %periodEvaluations Summary of this method goes here
            %   Detailed explanation goes here
            gamma_mean = ms.gamma_mean;
        end
        
        function [D_t_zz_s_mean] = mean_sliding_speed(ms)
            %getAveragedSwitchingRatios Summary of this method goes here
            %   Detailed explanation goes here
            D_t_zz_s_mean = ms.D_t_zz_s_mean;
        end
        
        function [x,u] = finalStates(ms)
            x = ms.zx_fin;
            x(end) = 0;
            u = ms.u_fin;
        end
    end
end

