classdef SlidingUnstable < Terminal
    %SlidingUnstable Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [cnd] = SlidingUnstable()
            %SlidingUnstable Construct an instance of this class
            %   Detailed explanation goes here
            cnd@Terminal("Sliding manifold is unstable.");
        end
    end
end

