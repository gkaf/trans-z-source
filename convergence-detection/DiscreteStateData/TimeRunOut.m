classdef TimeRunOut < Terminal
    %ConvergenceNotDetected Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [cnd] = TimeRunOut()
            %ConvergenceNotDetected Construct an instance of this class
            %   Detailed explanation goes here
            cnd@Terminal("Run out of simulation time.");
        end
    end
end

