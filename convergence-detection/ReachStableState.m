classdef ReachStableState < OutputFunctionsFlow & DiscreteStateFlow
    %UNTITLED26 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        epsilon
        % Info to detect unstable sliding surface
        Dt_burn_in
        Dt_cross
    end
    
    methods
        function [ds] = ReachStableState(burnInState, u, ...
                circuitProjection, ...
                epsilon, ...
                Dt_cross, ...
                Dt_burn_in, ...
                flowIntegrator, ...
                continuum_output_size)
            %ReachBoundary Construct an instance of this class
            %   Detailed explanation goes here
            ds@OutputFunctionsFlow(circuitProjection);
            ds@DiscreteStateFlow(SRCDetectionDiscreteStateEnumeration.ReachStableState, burnInState, u, ...
                flowIntegrator, Interval.FinalState, ...
                continuum_output_size)
            
            ds.epsilon = epsilon;
            ds.Dt_burn_in = Dt_burn_in;
            ds.Dt_cross = Dt_cross;
        end
        
        function [f, M] = flow(ds, ~ , ~) % ds, t, x
            n = ds.continuumOutputSize() - ds.compressedContinuumOutputSize();
            M = eye(n,n);
            
            u = ds.discreteOutput();
            f = ds.circuit.flow(u);
        end
        
        function [e, idx_terminal_bitmap] = event(ds, t, x) % ds, t, x
            % u = [u_1; u_2]
            u = ds.discreteOutput();
            
            if ismember(u(1), [0,1]) && ismember(u(2), [0,1])
                e = getEventsFcn();
            else
                e = {};
            end
            
            eps = ds.epsilon;
            t_init = ds.starting_time(x);
            t_state_init = t;
            Dt_cross_max = ds.Dt_cross;
            Dt_burn_in_max = ds.Dt_burn_in;
            
            idx_terminal_bitmap = [ones(size(eps)); 1; 1];
            direction_potential = [ones(size(eps)); -1; -1];
            
            function [e] = getEventsFcn()
                e = @eventsFcn;
                
                function [position, isterminal, direction] = eventsFcn(t, zx)
                    v = ds.circuit.potential_function(t, zx);
                    position = [v - eps; ...
                        Dt_burn_in_max - (t - t_init);
                        Dt_cross_max - (t - t_state_init)]; % The value that we want to be zero
                    isterminal = idx_terminal_bitmap; % Halt integration
                    direction = direction_potential;
                end
            end
        end
        
        function [stateTransition] = transition(ds, flowTranscript)
            e_idx = flowTranscript.e_idx();
            if isempty(e_idx)
                discreteStateData = TimeRunOut();
                stateTransition = StateTerminal(discreteStateData);
            else
                x_fin = flowTranscript.finalState();
                t_fin = flowTranscript.finalTimeInstance();
                zx_fin = ds.map(x_fin);
                burn_in_complete = false;
                
                u_new = ds.u;
                discreteStateData = {};
                wrong_idx = false;
                k = 1;
                while k <= length(e_idx) && isempty(discreteStateData) && ~wrong_idx
                    if e_idx(k) <= ds.circuit.n_surfaces
                        idx_S = e_idx(k);
                        switchingRule_cell = ds.circuit.critical_function(idx_S, t_fin, zx_fin, u_new);
                        
                        % Use the minimum critical value to avoid
                        % non-determinism.
                        N = length(switchingRule_cell);
                        v_crit = zeros(N,1);
                        for n = 1:N
                            v_crit(n) = switchingRule_cell{n}.critical_value();
                        end
                        [~, n] = min(v_crit);
                        
                        if isempty(n)
                            discreteStateData = SlidingUnstable();
                        else
                            u_new = switchingRule_cell{n}.get_u_post();
                        end
                    elseif e_idx(k) == ds.circuit.n_surfaces + 1
                        burn_in_complete = true;
                    elseif e_idx(k) == ds.circuit.n_surfaces + 2
                        if ~burn_in_complete
                            discreteStateData = ConvergedToSingleMode();
                        end
                    else
                        wrong_idx = true;
                    end
                    k = k+1;
                end
                
                if wrong_idx
                    stateTransition = {};
                elseif ~isempty(discreteStateData)
                    stateTransition = StateTerminal(discreteStateData);
                else
                    burnInState = ds.discreteStateData;
                    t_fin = flowTranscript.finalTimeInstance();
                    burnInState = burnInState.update(t_fin, zx_fin, u_new);
                    if burn_in_complete
                        evaluatingDutyRatios = burnInState.getEvaluatingDutyRatios();
                        
                        discreteStateEnumeration = SRCDetectionDiscreteStateEnumeration.ResetTime;
                        discreteStateData = evaluatingDutyRatios;
                        
                        stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_new);
                    else
                        discreteStateEnumeration = SRCDetectionDiscreteStateEnumeration.ReachStableState;
                        discreteStateData = burnInState;
                        
                        stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_new);
                    end
                end
            end
        end
    end
end

