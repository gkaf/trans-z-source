classdef SRCDetectionDiscreteStateGenerator < DiscreteStateGenerator
    %SRCDetectionDiscreteStateGenerator Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/map/CycleDetectionDiscreteStateGenerator.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        flowIntegrator
        % Memoized states
        circuitProjection
        
        % Controller hysteresis parameters
        epsilon
        
        % Evaluation parameters
        Dt_state_ub
        
        % Duty ratio convergence parameters
        eta % convergence threshold
        N_transient_extensions_ub
        Delta_t_burn_in_starting
        Delta_t_period_multiple_initial
    end
    
    methods
        function [dsg] = SRCDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
                circuitProjection, epsilon, ...
                eta, N_transient_extensions_ub, Delta_t_burn_in_starting, Delta_t_period_multiple_initial, ...
                Dt_state_ub)
            
            continuum_output_size = length(abs_tol) + circuitProjection.n_alternative + length(epsilon);
            dsg@DiscreteStateGenerator(continuum_output_size);
            
            dsg.flowIntegrator = DynamicFlowIntegrator(rel_tol, abs_tol, max_step);
            
            dsg.eta = eta;
            dsg.N_transient_extensions_ub = N_transient_extensions_ub;
            dsg.Delta_t_burn_in_starting = Delta_t_burn_in_starting;
            dsg.Delta_t_period_multiple_initial = Delta_t_period_multiple_initial;
            
            dsg.Dt_state_ub = Dt_state_ub;
            
            dsg.circuitProjection = circuitProjection;
            dsg.epsilon = epsilon;
        end
        
        function [discreteState] = synthesizeState(dsg, discreteStateEnumeration, discreteStateData, u, output_size)
            switch discreteStateEnumeration
                case SRCDetectionDiscreteStateEnumeration.StartConvergenceDetection
                    discreteState = StartConvergenceDetection(discreteStateData, u, ...
                        dsg.circuitProjection, output_size);
                case SRCDetectionDiscreteStateEnumeration.ReachStableState
                    discreteState = ReachStableState(discreteStateData, u, ...
                        dsg.circuitProjection, ...
                        dsg.epsilon, ...
                        dsg.Dt_state_ub, ...
                        dsg.Delta_t_burn_in_starting, ...
                        dsg.flowIntegrator, ...
                        output_size);
                case SRCDetectionDiscreteStateEnumeration.ResetTime
                    discreteState = ResetTime(discreteStateData, u, ...
                        dsg.circuitProjection, output_size);
                case SRCDetectionDiscreteStateEnumeration.EvaluateSwtchingRatios
                    discreteState = EvaluateSwtchingRatios(discreteStateData, u, ...
                        dsg.circuitProjection, ...
                        dsg.epsilon, ...
                        dsg.Dt_state_ub, ...
                        dsg.eta, dsg.N_transient_extensions_ub, dsg.Delta_t_period_multiple_initial, ...
                        dsg.flowIntegrator, ...
                        output_size);       
                case SRCDetectionDiscreteStateEnumeration.DetectSwitchingRatiosConvergence
                    discreteState = DetectSwitchingRatiosConvergence(discreteStateData, u, ...
                        dsg.circuitProjection, ...
                        dsg.epsilon, ...
                        dsg.Dt_state_ub, ...
                        dsg.flowIntegrator, ...
                        output_size);
                case SRCDetectionDiscreteStateEnumeration.CheckConvergence
                    discreteState = CheckConvergence(discreteStateData, u, ...
                        dsg.circuitProjection, output_size);
                otherwise
                    discreteState = {};
            end
        end
        
        function [dsg] = reset_sliding_state(dsg, zz_3)
            dsg.circuitProjection = dsg.circuitProjection.reset_sliding_state(zz_3);
        end
    end
    
end

