classdef CheckConvergence < OutputFunctions & DiscreteStateJump
    %CheckConvergence Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/Initial.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods
        function [ds] = CheckConvergence(detectingDutyRatiosConvergence, u, circuit, continuum_output_size)
            %CheckConvergence Construct an instance of this class
            %   Detailed explanation goes here
            
            ds@OutputFunctions(circuit);
            ds@DiscreteStateJump(SRCDetectionDiscreteStateEnumeration.CheckConvergence, detectingDutyRatiosConvergence, u, continuum_output_size);
            %flowIntegrator, interval, ...
        end
        
        function [e_idx] = guard(~, ~, ~) % ds, t, x
            e_idx = 1;
        end
        
        function [stateTransition, t_post, x_post] = jump(ds, ~, t_pre, x_pre) % ds, e_idx, t_pre, x_pre
            checkConvergence = ds;
            detectingDutyRatiosConvergence = ds.discreteStateData;
            if detectingDutyRatiosConvergence.converged()
                Delta_t_burn_in = detectingDutyRatiosConvergence.burn_in_period();
                Delta_t_period_multiple = detectingDutyRatiosConvergence.period_multiple();
                Delta_t_total = detectingDutyRatiosConvergence.total_time();
                
                gamma_mean = detectingDutyRatiosConvergence.mean_discrete_state();
                D_t_zz_s_mean = detectingDutyRatiosConvergence.mean_sliding_speed(checkConvergence);
                
                zx_fin = ds.continuumStateOutput(x_pre);
                u_fin = ds.u;
                
                discreteStateData = MeanStates(gamma_mean, D_t_zz_s_mean, ...
                    zx_fin, u_fin, ....
                    Delta_t_burn_in, Delta_t_period_multiple, Delta_t_total);
                
                stateTransition = StateTerminal(discreteStateData);
            else
                if detectingDutyRatiosConvergence.n_transient_extensions < detectingDutyRatiosConvergence.N_transient_extensions_ub
                    detectingDutyRatiosConvergence = detectingDutyRatiosConvergence.extend_transient_duration();
                else
                    detectingDutyRatiosConvergence = detectingDutyRatiosConvergence.extend_cycle_duration();
                end
                
                discreteStateEnumeration = SRCDetectionDiscreteStateEnumeration.ResetTime;
                discreteStateData = detectingDutyRatiosConvergence;
                u = ds.u;
                
                stateTransition = StateId(discreteStateEnumeration, discreteStateData, u);
            end
            
            t_post = t_pre;
            x_post = x_pre;
        end 
    end
    
    methods
        function [Delta_zz_3] = slide(ds, zx)
            Delta_zz_3 = zx(ds.main_end);
        end
    end
    
end


