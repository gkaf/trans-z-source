classdef SRCDetectionOutputHandler < OutputHandler
    %SRCDetectionOutputHandler Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n_main
    end
    
    methods
        function [oh] = SRCDetectionOutputHandler(n_main)
            %SRCDetectionOutputHandler Construct an instance of this class
            %   Detailed explanation goes here
            oh@OutputHandler();
            oh.n_main = n_main;
        end
        
        function [y] = reconstructContinuumOutput(oh, ~, x, zy)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            zx = x(1:oh.n_main);
            y = [zx; zy];
        end
    end
end

