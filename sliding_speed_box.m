function sliding_speed_box(filename, ZZ_3_s)
%sliding_speed Summary of this function goes here
%   Detailed explanation goes here
import_sliding_analysis

[dynamicsFunction, switchingFunction2Codim1, epsilon] = getDynamicsFunction2Codim1Surf();
circuitProjected = ZSourceBoxProj(dynamicsFunction, switchingFunction2Codim1);

zz_3_s_0 = ZZ_3_s(1);
slidingAnalysisResults = sliding_analysis(circuitProjected, epsilon, zz_3_s_0, ZZ_3_s);

SlidingAnalysis.save_results(slidingAnalysisResults, filename);

path(pathdef);
end

