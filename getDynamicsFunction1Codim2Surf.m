function [dynamicsFunction, switchingFunction1Codim2, epsilon] = getDynamicsFunction1Codim2Surf()
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
L = 1.5e-3;
n_turns = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;
i_ref = 2.4e3/E_load;
v_ref = 480;
r = ones(2,1).*0.01;
r_ESR = 0;
k = 32;
alpha = [384,1];

epsilon = 384*i_ref/4;

dynamicsFunction = DynamicsFunctionNonIdeal(L, n_turns, L_mag, C,...
    E_source, E_load, ...
    r, r_ESR, ...
    i_ref, v_ref, k);

switchingFunction1Codim2 = SwitchingFunction1Codim2(L, n_turns, L_mag, C, ...
    E_source, E_load, ...
    i_ref, v_ref, k,...
    alpha);
end

