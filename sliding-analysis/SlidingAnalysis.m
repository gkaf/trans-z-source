classdef SlidingAnalysis
    %SlidingSpeedPlot Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        e_tol
        max_step
        
        circuitProjection
        quadrant_number_of_points
        epsilon_hyst
        
        Dt_state_ub
        Dt_ub
        
        Delta_t_burn_in_initial
        
        N_transient_extensions_ub
        Delta_t_period_multiple_initial
        convergence_tolerance
    end
    
    methods
        function [ssp] = SlidingAnalysis(circuitProjection, ...
                quadrant_number_of_points, epsilon_hyst, ...
                Delta_t_burn_in_initial, N_transient_extensions_ub, Delta_t_period_multiple_initial, convergence_tolerance, ...
                Dt_state_ub, Dt_ub, e_tol, max_step)
            %SlidingAnalysis Construct an instance of this class
            %   Detailed explanation goes here
            ssp.circuitProjection = circuitProjection;
            ssp.quadrant_number_of_points = quadrant_number_of_points;
            ssp.epsilon_hyst = epsilon_hyst;
            
            ssp.Dt_state_ub = Dt_state_ub;
            ssp.Dt_ub = Dt_ub;
            
            ssp.Delta_t_burn_in_initial = Delta_t_burn_in_initial;
            ssp.N_transient_extensions_ub = N_transient_extensions_ub;
            ssp.Delta_t_period_multiple_initial = Delta_t_period_multiple_initial;
            ssp.convergence_tolerance = convergence_tolerance;
            
            ssp.e_tol = e_tol;
            ssp.max_step = max_step;
        end
        
        function [discreteStateGenerator] = getDiscreteStateGenerator(ssp)
            rel_tol = ssp.e_tol;
            abs_tol = ssp.e_tol .* ones(size(ssp.circuitProjection.nominal_state()));
            eta = ssp.convergence_tolerance;
            discreteStateGenerator = SRCDetectionDiscreteStateGenerator(rel_tol, abs_tol, ssp.max_step, ...
                ssp.circuitProjection, ssp.epsilon_hyst, ...
                eta, ssp.N_transient_extensions_ub, ssp.Delta_t_burn_in_initial, ssp.Delta_t_period_multiple_initial, ...
                ssp.Dt_state_ub);
        end
        
        function [result] = slidingSpeed(ssp, discreteStateGenerator, zz_3_s, t_0, x_0, u_0)
            %slidingSpeed Summary of this method goes here
            %   Detailed explanation goes here
            discreteStateGenerator = discreteStateGenerator.reset_sliding_state(zz_3_s);
            ha = HybridAutomaton(discreteStateGenerator);
            
            discreteStateData = {};
            initial_stateId = StateId(SRCDetectionDiscreteStateEnumeration.StartConvergenceDetection, discreteStateData, u_0);
            hae = HybridAutomatonEvaluation(ha, ssp.Dt_state_ub, ssp.Dt_ub, initial_stateId, t_0, x_0);
            
            result = hae.evaluate();
        end
        
        function [slidingAnalysisResults_series] = slidingSpeedContinuation(ssp, zz_3_s_init, ZZ_3)
            %slidingSpeed Summary of this method goes here
            %   Detailed explanation goes here
            discreteStateGenerator = ssp.getDiscreteStateGenerator();
            
            N = length(ZZ_3);
            slidingAnalysisResults_series = cell(N,1);
            
            t_0 = 0;
            x_0 = ssp.circuitProjection.nominal_state();
            u_0 = [1;1];
            
            result = ssp.slidingSpeed(discreteStateGenerator, zz_3_s_init, t_0, x_0, u_0);
            
            if isa(result, 'MeanStates')
                meanState = result;
                [x_0, u_0] = meanState.finalStates();
            end
            
            for n = 1:N
                zz_3_s = ZZ_3(n);
                
                tic;
                result = ssp.slidingSpeed(discreteStateGenerator, zz_3_s, t_0, x_0, u_0);
                Dt = toc;
                
                if isa(result, 'MeanStates')
                    meanState = result;
                    slidingAnalysisResults_series{n} = WellDefinedSlidingSpeed(meanState, zz_3_s, Dt);
                    
                    [x_0, u_0] = meanState.finalStates();
                else
                    slidingAnalysisResults_series{n} = NotWellDefinedSlidingSpeed(result, zz_3_s, Dt);
                end
            end
        end
    end
    
    
    methods (Static)
        function save_results(slidingAnalysisResults, filename)
            %UNTITLED2 Summary of this function goes here
            %   Detailed explanation goes here
            N = length(slidingAnalysisResults);
            
            ZZ_3_s = zeros(N,1);
            mean_D_t_zz = zeros(N,1);
            mean_gamma = zeros(N,4);
            result_description = cell(N,1);
            evaluation_time = zeros(N,1);
            burn_in = zeros(N,1);
            period_multiple = zeros(N,1);
            total = zeros(N,1);
            
            for n = 1:N
                ZZ_3_s(n) = slidingAnalysisResults{n}.sliding_state();
                mean_D_t_zz(n) = slidingAnalysisResults{n}.averaged_sliding_speed();
                result_description{n} = slidingAnalysisResults{n}.describe();
                
                evaluation_time(n) = slidingAnalysisResults{n}.get_evaluation_times();
                
                if isa(slidingAnalysisResults{n}, 'WellDefinedSlidingSpeed')
                    burn_in(n) = slidingAnalysisResults{n}.Delta_t_burn_in;
                    period_multiple(n) = slidingAnalysisResults{n}.Delta_t_period_multiple;
                    total(n) = slidingAnalysisResults{n}.Delta_t_total;
                else
                    burn_in(n) = NaN;
                    period_multiple(n) = NaN;
                    total(n) = NaN;
                end
                
                gamma = slidingAnalysisResults{n}.averaged_control_input();
                mean_gamma(n,:) = transpose(gamma);
            end
            
            T = table(ZZ_3_s, mean_D_t_zz, mean_gamma(:,1), mean_gamma(:,2), mean_gamma(:,3), mean_gamma(:,4), result_description, evaluation_time, burn_in, period_multiple, total, ...
                'VariableNames', {'z', 'dz', 'gamma_00', 'gamma_10', 'gamma_01', 'gamma_11', 'Type', 'Dt_cpu', 'burn_in', 'period_multiple', 'total'});
            writetable(T,filename,'WriteVariableNames',true,'Delimiter',';');
        end
    end
end

