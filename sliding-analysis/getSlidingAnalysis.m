function [slidingAnalysis] = getSlidingAnalysis(circuitProjection, epsilon_hyst)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

quadrant_number_of_points = 100;
Dt_state_ub = 0.004;
Dt_ub = 8;
e_tol = 1e-12;
max_step = 1e-6;

Delta_t_burn_in_initial = 0.008;
Delta_t_period_multiple_initial = 0.032;
convergence_tolerance = 0.5e-4;
N_transient_extensions_ub = 4;


slidingAnalysis = SlidingAnalysis(circuitProjection, ...
    quadrant_number_of_points, epsilon_hyst, ...
    Delta_t_burn_in_initial, N_transient_extensions_ub, Delta_t_period_multiple_initial, convergence_tolerance, ...
    Dt_state_ub, Dt_ub, e_tol, max_step);
end

