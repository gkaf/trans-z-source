classdef WellDefinedSlidingSpeed < SlidingAnalysisResult
    %WellDefinedSlidingSpeed Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        D_t_zz_3
        gamma_mean
        
        Delta_t_burn_in
        Delta_t_period_multiple
        Delta_t_total
    end
    
    methods
        function [sar] = WellDefinedSlidingSpeed(meanStates, zz_3, evaluation_times)
            %UNTITLED5 Construct an instance of this class
            %   Detailed explanation goes here
            sar@SlidingAnalysisResult(meanStates, zz_3, evaluation_times)
            
            if ~isa(meanStates, 'MeanStates')
                msgID = 'WellDefinedSlidingSpeed:Exception';
                msgtext = 'Expecting well defined sliding vector in a MeanState object.';
                ME = MException(msgID, msgtext);
                throw(ME);
            end
            
            sar.gamma_mean = meanStates.mean_discrete_state();
            sar.D_t_zz_3 = meanStates.mean_sliding_speed();
            
            sar.Delta_t_burn_in = meanStates.Delta_t_burn_in;
            sar.Delta_t_period_multiple = meanStates.Delta_t_period_multiple;
            sar.Delta_t_total = meanStates.Delta_t_total;
        end
        
        function [D_t_z] = averaged_sliding_speed(sar)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            D_t_z = sar.D_t_zz_3;
        end
        
        function [gamma] = averaged_control_input(sar)
            %averaged_control_input Summary of this method goes here
            %   Output: [gamma_00, gamma_10, gamma_01, gamma_11]
            N = size(sar.gamma_mean,1) * size(sar.gamma_mean,2);
            gamma = zeros(N,1);
            for n = 1:size(sar.gamma_mean,2)
                for m = 1:size(sar.gamma_mean,1)
                    k = 2*(n-1) + (m-1) + 1;
                    gamma(k) = sar.gamma_mean(m,n);
                end
            end
        end
        
        function [n] = iterations(sar)
            n = sar.n_iterations;
        end
    end
end

