classdef NotWellDefinedSlidingSpeed < SlidingAnalysisResult
    %WellDefinedSlidingSpeed Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [sar] = NotWellDefinedSlidingSpeed(finalResult, zz_3, evaluation_times)
            %UNTITLED5 Construct an instance of this class
            %   Detailed explanation goes here
            sar@SlidingAnalysisResult(finalResult, zz_3, evaluation_times)
        end
        
        function [D_t_z] = averaged_sliding_speed(~)
            %averaged_sliding_speed Summary of this method goes here
            %   Detailed explanation goes here
            D_t_z = NaN;
        end
        
        function [gamma] = averaged_control_input(~)
            gamma = ones(4,1).*NaN;
        end
        
        function [n] = iterations(~)
            n = NaN;
        end
    end
end

