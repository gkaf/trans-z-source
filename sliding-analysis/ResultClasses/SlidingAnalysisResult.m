classdef (Abstract) SlidingAnalysisResult
    %SlidingAnalysisResult Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        result
        zz_3
        % Result description sring field
        description
        % Metadata
        evaluation_times
    end
    
    methods (Abstract)
        [D_t_z] = averaged_sliding_speed(sar);
        [n] = iterations(sar);
        [gamma] = averaged_control_input(sar);
    end
    
    methods
        function [sar] = SlidingAnalysisResult(result, zz_3, evaluation_times)
            %SlidingAnalysisResult Construct an instance of this class
            %   Detailed explanation goes here
            sar.result = result;
            sar.zz_3 = zz_3;
            sar.evaluation_times = evaluation_times;
            
            sar.description = class(result);
        end
        
        function [str] = describe(sar)
            %describe Summary of this method goes here
            %   Detailed explanation goes here
            str = sar.description;
        end
        
        function [zz_3] = sliding_state(sar)
            zz_3 = sar.zz_3;
        end
        
        function [Dt] = get_evaluation_times(sar)
            Dt = sar.evaluation_times;
        end
    end
end

