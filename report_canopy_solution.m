function [kappa_root, zz_3_feas, D_t_zz_3, del_zz_3_D_t_zz_3, u, m, D, del_zz_3_D_t_zz_3_parametric, kappa] = report_canopy_solution(fld)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
[dynamicsFunction, switchingFunction1Codim2, epsilon] = getDynamicsFunction1Codim2SurfFlat();

solver_tolerance = 1e-12;
ansol = AnalyticalSolutions(dynamicsFunction, ...
    switchingFunction1Codim2, epsilon, ...
    solver_tolerance);

zz_3_s = 0;
kappa = transpose(-2:0.1:48);
del_zz_3_D_t_zz_3_parametric = ansol.canopy_gradiant_parametric_analysis(zz_3_s, kappa);

zz_3 = transpose(-120:0.1:120);
[zz_3_feas, D_t_zz_3, u, m, D] = ansol.canopy_speed(zz_3);

del_zz_3_D_t_zz_3 = ansol.canopy_gradiant(zz_3_feas);

T = table(kappa, del_zz_3_D_t_zz_3_parametric, ...
    'VariableNames', {'k', 'lambda'});

filename = [fld, '/', 'data/eigenvalues.dat'];
writetable(T,filename,'WriteVariableNames',true,'Delimiter',';');

T_cp = table(zz_3_feas(:), D_t_zz_3(:), u(1,:)', u(2,:)', m(:), D(:), del_zz_3_D_t_zz_3(:), ...
    'VariableNames', {'zz_3_feas', 'D_t_zz_3', 'u_1', 'u_2', 'm', 'D', 'del_zz_3_D_t_zz_3'});

filename_cp = [fld, '/', 'data/canopy_ideal.dat'];
writetable(T_cp,filename_cp,'WriteVariableNames',true,'Delimiter',';');

kappa_root = ansol.canopy_gradiant_denominator_root();

end

