classdef DynamicsFunctionIdeal < DynamicsFunction
    %DynamicsFunctionFlat Ideal dynamics for the trans-Z-source inverter
    %   The class constructs the time derivative for the state of the ideal
    %   converter.
    
    properties
    end
    
    methods
        function [df] = DynamicsFunctionIdeal(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k)
            %DynamicsFunctionFlat Construct an instance of this class
            %   Detailed explanation goes here
            df@DynamicsFunction(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k);
        end
        
        function [dx] = f_der(df, ~, x, u)
            %f_der Time derivative of the ideal converter state
            %   Time derivative of the ideal converter state. The system is
            %   autonomous, and the control input is
            %   - u = [u_{+}, u_{-}],
            %   abd the states of the system are
            %   - x = [i_{L}, i_{L_{f}}, v_{C}].
            di_L = (1/df.L)*(-df.E(2) + (u(1)-u(2))*(x(3) + (1/df.n_turns)*(x(3) - df.E(1))));
            di_L_mag = (1/df.L_mag)*((1-u(1)*u(2))*(1/df.n_turns)*(df.E(1) - x(3)) + u(1)*u(2)*x(3));
            dv_C = (1/df.C)*(-u(1)*u(2)*x(2) + (1 - u(1)*u(2))*(1/df.n_turns)*x(2) - (1+1/df.n_turns)*(u(1) - u(2))*x(1) );
            
            
            dx = [di_L; di_L_mag; dv_C];
        end
    end
end

