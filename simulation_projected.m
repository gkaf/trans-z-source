% z-source/simulation_projected.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

import_flow;

[dynamicsFunction, switchingFunction1Codim2, epsilon] = getDynamicsFunction1Codim2Surf();
circuit = ZSourceProj(dynamicsFunction, switchingFunction1Codim2);
zz_3_s = 60;
circuit = circuit.reset_sliding_state(zz_3_s);

Dt_fin = 0.001; %2*0.01;
Dt_burn_in = 0.02; %0;

e_tol = 1e-12;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;
state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, ...
    Dt_fin, Dt_burn_in);

ha = HybridAutomaton(state_gen);

Dt_state_ub = 0.02;
Dt_ub = 1.6;
t_0 = 0;
% Origninal cycle:
x_0 = circuit.nominal_state();
% Same as original 
% x_0 = [-1;-40;0];
% Different from original
%x_0 = [1;20;0];

discreteStateData = Transcript(state_gen);
u_0 = [1;1];

initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

transcript = hae.evaluate();
 
[ts, ys, us] = transcript.get_tyu();

path(pathdef);