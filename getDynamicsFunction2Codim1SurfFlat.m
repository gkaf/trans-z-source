% z-source/getDynamicsFunction2Codim1SurfFlat.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2019
% Contact: georgios.kafanas@bristol.ac.uk

function [dynamicsFunction, switchingFunction2Codim1, epsilon] = getDynamicsFunction2Codim1SurfFlat()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
L = 1.5e-3;
n_turns = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;
i_ref = 2.4e3/E_load;
v_ref = 480;
k = 32;

dynamicsFunction = DynamicsFunctionIdeal(L, n_turns, L_mag, C,...
    E_source, E_load, ...
    i_ref, v_ref, k);

[switchingFunction2Codim1, ~, ~] = SwitchingFunction2Codim1.default_generate(L, n_turns, L_mag, C, ...
    E_source, E_load, ...
    i_ref, v_ref, k);

%epsilon = [3; 4].^2;
%epsilon = [2; 1.6].^2;
%epsilon = [2; 128].^(1/2);
epsilon = [1.42; 11.4];
end