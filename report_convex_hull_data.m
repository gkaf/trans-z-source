function report_convex_hull_data(root_fld)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

hull_data_folder = [root_fld, '/', 'convex-hull/convex-hull-hysteresis-extended'];
simulation_data_folder = [root_fld, '/', 'sliding-speed'];

T_speed = readtable([simulation_data_folder, '/', 'sliding_speed_box.dat'], 'Delimiter', ';');
T_hull = readtable([hull_data_folder, '/', 'hysteresis_box_convex_hull.dat'], 'Delimiter', ';');


[z, Dz, Delta_Dz_max, Delta_Dz_min, Delta_Dz] = get_vectors(T_hull, T_speed);
T = table(z, Dz, Delta_Dz_max, Delta_Dz_min, Delta_Dz, ...
    'VariableNames', {'z_box', 'Dz_box', 'Delta_Dz_max_box', 'Delta_Dz_min_box', 'Delta_Dz_box'});
filename = [hull_data_folder, '/', 'box_hysteresis_convex_hull_range.dat'];
writetable(T, filename, 'WriteVariableNames', true, 'Delimiter', ';');

T_speed = readtable([simulation_data_folder, '/', 'sliding_speed.dat'], 'Delimiter', ';');
T_hull = readtable([hull_data_folder, '/', 'hysteresis_ellipse_convex_hull.dat'], 'Delimiter', ';');

[z, Dz, Delta_Dz_max, Delta_Dz_min, Delta_Dz] = get_vectors(T_hull, T_speed);
T = table(z, Dz, Delta_Dz_max, Delta_Dz_min, Delta_Dz, ...
    'VariableNames', {'z_ellipse', 'Dz_ellipse', 'Delta_Dz_max_ellipse', 'Delta_Dz_min_ellipse', 'Delta_Dz_ellipse'});
filename = [hull_data_folder, '/', 'ellipse_hysteresis_convex_hull_range.dat'];
writetable(T, filename, 'WriteVariableNames', true, 'Delimiter', ';');

    function [z, Dz, Delta_Dz_max, Delta_Dz_min, Delta_Dz] = get_vectors(T_hull, T_speed)
        z_min = max(min(T_hull.zz_3_feas), min(T_speed.z));
        
        flt_hull = T_hull.zz_3_feas > z_min;
        flt_speed = T_speed.z > z_min;
        
        z = T_hull.zz_3_feas(flt_hull);
        Dz = T_speed.dz(flt_speed);
        Delta_Dz_max = T_hull.f_sld_max(flt_hull) - T_hull.D_t_zz_3(flt_hull);
        Delta_Dz_min = T_hull.f_sld_min(flt_hull) - T_hull.D_t_zz_3(flt_hull);
        Delta_Dz = Dz - T_hull.D_t_zz_3(flt_hull);
    end
end

