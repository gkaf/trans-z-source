classdef DynamicsFunctionNonIdeal < DynamicsFunction
    %DynamicsFunctionNonIdeal Non-ideal dynamics for the trans-Z-source
    %inverter
    %   The class constructs the time derivative for the state of a
    %   non-ideal converter where the equivalent series resistance losses
    %   in the semiconductor switches of the phase bridge and the
    %   equivalent series resistance losses in the transformer are
    %   introduced into the model.
    
    properties
        r
        r_ESR
    end
    
    methods
        function [df] = DynamicsFunctionNonIdeal(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                r, r_ESR, ...
                i_ref, v_ref, k)
            %DynamicsFunctionNonIdeal Construct an instance of this class
            %   Detailed explanation goes here
            df@DynamicsFunction(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k);
            df.r = r;
            df.r_ESR = r_ESR;
        end
        
        function [dx] = f_der(df, ~, x, u)
            %f_der Time derivative of the stat of the converter with
            %non-ideal dynamics.
            %   Time derivative of the state of the converter with
            %   non-ideal dynamics. The equivalent series resitance of the
            %   switches and the transformer are considered in this model.
            %   The system is autonomous; the control input is
            %   - u = [u_{+}, u_{-}],
            %   and the states of the system are
            %   - x = [i_{L}, i_{L_{f}}, v_{C}].
            
            % Resistance to the output current
            r_L = (1-u(1))*u(2)*df.r(1) + u(1)*(1-u(2))*df.r(2) + u(1)*u(2)*(df.r(1)*df.r(2)/(df.r(1)+df.r(2)));
            % Resistance to the magnetizing current
            r_Lm = df.r_ESR + u(1)*u(2)*(df.r(1)*df.r(2)/(df.r(1)+df.r(2)));
            
            di_L = (1/df.L)*(-df.E(2) + (u(1)-u(2))*(x(3) + (1/df.n_turns)*(x(3) - df.E(1))) - x(1)*r_L);
            di_L_mag = (1/df.L_mag)*((1-u(1)*u(2))*(1/df.n_turns)*(df.E(1) - x(3)) + u(1)*u(2)*x(3) - x(2)*r_Lm);
            dv_C = (1/df.C)*(-u(1)*u(2)*x(2) + (1 - u(1)*u(2))*(1/df.n_turns)*x(2) - (1+1/df.n_turns)*(u(1) - u(2))*x(1) );
            
            dx = [di_L; di_L_mag; dv_C];
        end
    end
end

