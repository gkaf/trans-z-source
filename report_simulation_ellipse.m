function [T_time, T_phase] = report_simulation_ellipse(root_fld)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

result_fld = [root_fld, '/data/trans-Z-source/simulations'];
[ts, ys, us] = evaluate_trajecotry();
ts = ts - ts(1) .* ones(size(ts));

T_time = table(ts', ...
    ys(1,:)', ys(2,:)', ys(3,:)', ys(4,:)', ys(5,:)', ys(6,:)', ys(7,:)', ...
    us(1,:)', us(2,:)', ...
    'VariableNames', {'t', 'i_L', 'i_L_m', 'v_C', 'h_1', 'h_2', 'z', 'V', 'u_1', 'u_2'});

idx_crit = floor((2/9)*length(ts));
ys = ys(:, 1:idx_crit);

T_phase = table(ys(4,:)', ys(5,:)', ...
    'VariableNames', {'h_1', 'h_2'});

if ~isempty(root_fld)
    filename = [result_fld, '/', 'ellipse_phaseplot.dat'];
    writetable(T_phase, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [result_fld, '/', 'ellipse_timeplot.dat'];
    writetable(T_time, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

    function [ts, ys, us] = evaluate_trajecotry()
        import_flow;
        
        [dynamicsFunction, switchingFunction1Codim2, epsilon] = getDynamicsFunction1Codim2Surf();
        circuit = ZSource(dynamicsFunction, switchingFunction1Codim2);
        
        Dt_fin = 0.001;
        Dt_burn_in = 0.08;
        
        e_tol = 1e-8;
        rel_tol = e_tol;
        abs_tol = e_tol .* ones(size(circuit.nominal_state()));
        max_step = 1e-5;
        state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
            circuit, ...
            epsilon, ...
            Dt_fin, Dt_burn_in);
        
        ha = HybridAutomaton(state_gen);
        
        Dt_state_ub = 0.02;
        Dt_ub = 1.6;
        % Selecting initial state on the sliding mode:
        t_0 = 0;
        x_0 = circuit.nominal_state();
        
        discreteStateData = Transcript(state_gen);
        u_0 = [1;1];
        
        initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
        hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);
        
        transcript = hae.evaluate();
        
        [ts, ys, us] = transcript.get_tyu();
        
        path(pathdef);
    end

end

