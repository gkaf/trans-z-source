classdef MaxConverganceRate < SwitchingRule
    %MaxConverganceRate Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        DtV
    end
    
    methods
        function [sr] = MaxConverganceRate(idx_S, t_fin, zx_fin, u_post, DtV)
            %MaxConverganceRate Construct an instance of this class
            %   Detailed explanation goes here
            sr@SwitchingRule(idx_S, t_fin, zx_fin, u_post);
            sr.DtV = DtV;
        end
        
        function [v_crit] = critical_function(sr, idx_S, ~, ~, ~) % sr, idx_S, zx_fin, u_post
            %critical_function Summary of this method goes here
            %   Detailed explanation goes here
            v_crit = sr.DtV(idx_S);
        end
    end
end

