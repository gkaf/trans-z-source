classdef ZSource < CircuitPhysical
    %ZSource Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        U
        num_U
    end
    
    methods
        function [zsc] = ZSource(dynamicsFunction, switchingFunction1Codim2)
            %ZSource Construct an instance of this class
            %   Detailed explanation goes here
            n_surfaces = 1;
            n_main = 3;
            n_alternative = 3;
            
            zx_ref = switchingFunction1Codim2.zx_ref;
            
            zsc@CircuitPhysical(dynamicsFunction, switchingFunction1Codim2, ...
                n_surfaces, n_main, n_alternative, ....
                zx_ref);
            
            zsc.U = dynamicsFunction.U;
            zsc.num_U = length(zsc.U);
        end
        
        function [switchingRule_cell] = critical_function(zsc, idx_S, t_fin, zx_fin, ~) % zsc, idx_S, t_fin, zx_fin, u
            switchingRule_Stack = Stack();
            idx = 1;
            while idx <= zsc.num_U
                u_post = zsc.idx2ctrl(idx);
                DtV = zsc.potential_derivative(t_fin, zx_fin, u_post);
                if DtV < 0
                    sr = MaxConverganceRate(idx_S, t_fin, zx_fin, u_post, DtV);
                    switchingRule_Stack = switchingRule_Stack.push(sr);
                end
                idx = idx+1;
            end
                    
            switchingRule_cell = switchingRule_Stack.toCell();
        end
        
        function [zz] = alternative_space(zsc, zx)
            zz = zsc.dynamicsFunction.normal_space(zx);
        end
        
        function [u] = idx2ctrl(zsc, idx)
            u = zsc.U(:, idx);
        end
    end
end

