classdef ZSourceBox < CircuitPhysical
    %ZSource Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [zsc] = ZSourceBox(dynamicsFunction, switchingFunction2Codim1)
            %ZSource Construct an instance of this class
            %   Detailed explanation goes here
            n_surfaces = 2;
            n_main = 3;
            n_alternative = 3;
            
            zx_ref = switchingFunction2Codim1.zx_ref;
            
            zsc@CircuitPhysical(dynamicsFunction, switchingFunction2Codim1, ...
                n_surfaces, ...
                n_main, n_alternative, ...
                zx_ref);
        end
        
        function [switchingRule_cell] = critical_function(zsc, idx_S, t_fin, zx_fin, w_pre) % zsc, idx_S, zx_fin, u
            fds = zsc.switchingFunction;
            
            s = fds.S*fds.switching_function(t_fin, zx_fin);
            w = s <= 0;
            
            switchingRule_Stack = Stack();
            
            %DtV = fds.potential_derivative(t_fin, zx_fin, h, u_post);
            DtV = zeros(size(w));
            for n_idx = 1:length(DtV)
                DtV(n_idx) = (2*w(n_idx) - 1)*s(n_idx);
            end
            if DtV(idx_S) < 0
                w_post = w_pre;
                w_post(idx_S) = w(idx_S);
                sr = MaxConverganceRate(idx_S, t_fin, zx_fin, w_post, DtV);
                switchingRule_Stack = switchingRule_Stack.push(sr);
            end
            
            switchingRule_cell = switchingRule_Stack.toCell();
        end
        
        function [zz] = alternative_space(zsc, zx)
            zz = zsc.switchingFunction.normal_space(zx);
        end
    end
end

