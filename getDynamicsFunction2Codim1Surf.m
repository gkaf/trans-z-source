% z-source/getDynamicsFunction2Codim1Surf.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2019
% Contact: georgios.kafanas@bristol.ac.uk

function [dynamicsFunction, switchingFunction2Codim1, epsilon] = getDynamicsFunction2Codim1Surf()
%getDynamicsFunction2Codim1Surf Function generating an elliptical switching
%surface
%   The function generates the dynamics for the trans-Z-source converter,
%   and an elliptical switching surface with the associated logic for its
%   modulation.
L = 1.5e-3;
n_turns = 2;
L_mag = 1e-3;
C = 48e-6;
E_source = 100;
E_load = 380;
i_ref = 2.4e3/E_load;
v_ref = 480;
r = ones(2,1).*0.01;
r_ESR = 0;
k = 32;

dynamicsFunction = DynamicsFunctionNonIdeal(L, n_turns, L_mag, C,...
    E_source, E_load, ...
    r, r_ESR, ...
    i_ref, v_ref, k);

[switchingFunction2Codim1, ~, ~] = SwitchingFunction2Codim1.default_generate(L, n_turns, L_mag, C, ...
    E_source, E_load, ...
    i_ref, v_ref, k);

%epsilon = [3; 4].^2;
%epsilon = [2; 1.6].^2;
%epsilon = [2; 128].^(1/2);
epsilon = [1.42; 11.4];
end

