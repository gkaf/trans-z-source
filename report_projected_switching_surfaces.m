function report_projected_switching_surfaces(fld)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[~, switchingFunction1Codim2, epsilon_1] = getDynamicsFunction1Codim2Surf();
[~, switchingFunction2Codim1, epsilon_2] = getDynamicsFunction2Codim1Surf();

tolerance = 1e-12;
switchingSurface2Codim1 = SwitchingSurface2Codim1(switchingFunction2Codim1);
switchingSurface1Codim2 = SwitchingSurface1Codim2(switchingFunction1Codim2, tolerance);

N = 200;

SZX = switchingSurface1Codim2.segment_plotable_hysteresis_manifolds_projected_space(N, epsilon_1);
file_name = [fld, '/', 'data/projected_switching_surfaces/ellipse/ellipse.dat'];

save_vector = transpose(SZX{1});
dlmwrite(file_name, save_vector, 'delimiter', ';');

SZX = switchingSurface2Codim1.segment_plotable_hysteresis_manifolds_projected_space(N, epsilon_2);

file_name = [fld, '/', 'data/projected_switching_surfaces/box/box_surf1.dat'];

save_vector = transpose(SZX{1,1});
dlmwrite(file_name, save_vector, 'delimiter', ';');

fileID = fopen(file_name, 'a');
fprintf(fileID, '\n');
fclose(fileID);

save_vector = transpose(SZX{2,1});
dlmwrite(file_name, save_vector, 'delimiter', ';', '-append');

file_name = [fld, '/', 'data/projected_switching_surfaces/box/box_surf2.dat'];

save_vector = transpose(SZX{1,2});
dlmwrite(file_name, save_vector, 'delimiter', ';');

fileID = fopen(file_name, 'a');
fprintf(fileID, '\n');
fclose(fileID);

save_vector = transpose(SZX{2,2});
dlmwrite(file_name, save_vector, 'delimiter', ';', '-append');

end

