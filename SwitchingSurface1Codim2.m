classdef SwitchingSurface1Codim2 < SwitchingSurface
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % solver paramteres
        solver_options
    end
    
    methods
        function [df] = SwitchingSurface1Codim2(switchingFunction1Codim2, tolerance)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            df@SwitchingSurface(switchingFunction1Codim2);
            % Solver options for determining the switching manifold
            df.solver_options = optimoptions('fsolve', 'Display', 'off', 'FunctionTolerance', tolerance);
        end
        
        function [SZX] = segment_plotable_hysteresis_manifolds_projected_space(fds, N, epsilon)
            SZX = {fds.plotable_hysteresis_manifolds_projected_space(N, epsilon)};
        end
        
        function [SZX] = plotable_hysteresis_manifolds_projected_space(fds, N, epsilon)
            SZX = fds.hysteresis_manifolds_projected_space(N, epsilon);
            S = SZX{1};
            S_0 = S(:,1);
            S_new = [S, S_0]; % End with index 1 to complete cycle!
            SZX = S_new;
        end
        
        function [SZX] = hysteresis_manifolds_projected_space(fds, N, epsilon)
            % The funtion returns a line of points that is the solution set
            % of V(x) = epsilon, where V is some (shifted) quadtratic
            % Lyapunov function. The process utilized the symmetry of such
            % functions.
            
            Alpha = fds.switchingFunction.Alpha;
            A_scale = Alpha^(1/2); % Note that Alpha is symmetric since it is PD
            dphi = (pi/2)/(N+1);
            Phi = 0:dphi:(pi/2);
            
            ZX = zeros(2, length(Phi));
            zx_0 = [epsilon; 0];
            for n = 1:length(Phi)
                phi = Phi(n);
                intersection = zero_ax(phi);
                [zx, ~, ~, ~] = fsolve(intersection, zx_0, fds.solver_options); % x, fval, exitflag, output
                zx = abs(zx);
                
                ZX(:,n) = zx;
                zx_0 = zx;
            end
            
            % Exploit symmetry (quadrants numbered from 0)
            m = length(Phi)-1;
            ZX_0 = ZX(:,1:end-1);
            ZX_1 = [-1*ones(1,m); ones(1,m)].*flip(ZX(:,2:end), 2);
            ZX_2 = [-1*ones(1,m); -1*ones(1,m)].*ZX(:,1:end-1);
            ZX_3 = [ones(1,m); -1*ones(1,m)].*flip(ZX(:,2:end), 2);
            SZX = {[ZX_0, ZX_1, ZX_2, ZX_3]};
            
            function [f] = zero_ax(phi)
                f = @intersection;
                function [h] = intersection(zx)
                    h_2 = [-sin(phi), cos(phi)]*(A_scale*zx);
                    h = [V(zx), h_2];
                end
            end
            
            function [v] = V(zx)
                v = (1/2)*transpose(zx)*Alpha*zx - epsilon;
            end
        end
        
        function [flag] = manifold_point_admissible(fds, ~, zx) % fds, idx, zx
            U = fds.control_inputs();
            K = size(U, 2);
            
            admissible_vector_field_exists = false;
            k = 1;
            while k <= K && ~admissible_vector_field_exists
                u = U(:,k);
                DtS = fds.potential_derivative(zx, u);
                admissible_vector_field_exists = DtS < 0;
                k = k + 1;
            end
            
            flag = admissible_vector_field_exists;
        end
    end
    
    methods (Static)
        function plot_surface_set(S)
            M_len = size(S,3);
            figure
            hold on
            for m = 1:M_len
                plot(S(1,:,m), S(2,:,m), 'b');
            end
            hold off
        end
    end
end

