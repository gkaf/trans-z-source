% z-source/sliding_analysis.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk
function [slidingAnalysisResults] = sliding_analysis(circuitProjected, epsilon, zz_3_s_0, ZZ_3_s)

slidingAnalysis = getSlidingAnalysis(circuitProjected, epsilon);
slidingAnalysisResults = slidingAnalysis.slidingSpeedContinuation(zz_3_s_0, ZZ_3_s);

end

