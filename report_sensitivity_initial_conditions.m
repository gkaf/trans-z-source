function [T_original, T_shifted] = report_sensitivity_initial_conditions(root_fld)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

result_fld = [root_fld, '/data/trans-Z-source/limit-cycle-analysis/initial-conditions'];

x_0 = [0;0];
[ts, ys, ~] = evaluate_trajecotry(x_0);

idx_crit = floor((2/9)*length(ts));
ys = ys(:, 1:idx_crit);

T_original = table(ys(1,:)', ys(2,:)', ...
    'VariableNames', {'h_1', 'h_2'});

x_0 = [1;20];
[ts, ys, ~] = evaluate_trajecotry(x_0);

idx_crit = floor((2/9)*length(ts));
ys = ys(:, 1:idx_crit);

T_shifted = table(ys(1,:)', ys(2,:)', ...
    'VariableNames', {'h_1', 'h_2'});


if ~isempty(root_fld)
    filename = [result_fld, '/', 'ellipse_original.dat'];
    writetable(T_original, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [result_fld, '/', 'ellipse_shifted.dat'];
    writetable(T_shifted, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

    function [ts, ys, us] = evaluate_trajecotry(zx_0)
        import_flow;
        
        [dynamicsFunction, switchingFunction1Codim2, epsilon] = getDynamicsFunction1Codim2Surf();
        circuit = ZSourceProj(dynamicsFunction, switchingFunction1Codim2);
        zz_3_s = 0;
        circuit = circuit.reset_sliding_state(zz_3_s);
        
        Dt_fin = 0.002; %2*0.01;
        Dt_burn_in = 0.02; %0;
        
        e_tol = 1e-12;
        rel_tol = e_tol;
        abs_tol = e_tol .* ones(size(circuit.nominal_state()));
        max_step = 1e-5;
        state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
            circuit, ...
            epsilon, ...
            Dt_fin, Dt_burn_in);
        
        ha = HybridAutomaton(state_gen);
        
        Dt_state_ub = 0.02;
        Dt_ub = 1.6;
        t_0 = 0;
        x_0 = [zx_0;0];
        
        discreteStateData = Transcript(state_gen);
        u_0 = [1;1];
        
        initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
        hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);
        
        transcript = hae.evaluate();
        
        [ts, ys, us] = transcript.get_tyu();
        
        path(pathdef);
    end

end

