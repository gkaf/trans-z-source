function report_projected_vector_fileds(fld)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
[dynamicsFunction, ~, ~] = getDynamicsFunction1Codim2SurfFlat();

U = dynamicsFunction.U;
zx_ref = dynamicsFunction.zx_ref;
J_H = dynamicsFunction.J_H;

dx = zeros(3,size(U,2));
for n = 1:size(U,2)
    dx(:,n) = dynamicsFunction.f_der([], zx_ref, U(:,n));
end
H_dx = J_H*dx;

save_vector = [1e-6*transpose(H_dx), transpose(U)];

file_name = [fld, '/', 'data/vector_fields.dat'];

dlmwrite(file_name,save_vector,';');

end