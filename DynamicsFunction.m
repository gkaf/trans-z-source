classdef (Abstract) DynamicsFunction
    %DynamicsFunction Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/DynamicsFunction.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % Parameters
        k
        M
        D_s
        i_L_mag_ref
        J_H
        
        % Maps
        T_trs
        T_inv
        zx_ref
        
        % Dynamics parameters
        L
        L_mag
        C
        E
        n_turns
        
        % Available control vectors
        U
    end
    
    methods (Abstract)
        [dx] = f_der(fsd, t, x, u);
    end
    
    methods
        function [fds] = DynamicsFunction(L, n_turns, L_mag, C, ...
                E_source, E_load, ...
                i_ref, v_ref, k)
            %DynamicsFunction Construct an instance of this class
            %   Detailed explanation goes here
            fds.U = [[0;0], [1;0], [0;1], [1;1]];
            
            fds.k = k;
            fds.J_H = [1, 0, 0;
                0, k, 1];
            
            null_J_H = null(fds.J_H);
            for idx = 1:size(null_J_H, 2)
                nrm = norm(null_J_H(:,idx), 2);
                null_J_H(idx,:) = null_J_H(idx,:).*(1/(nrm^2));
            end
            
            fds.T_trs = [fds.J_H; ctranspose(null_J_H)];
            fds.T_inv = inv(fds.T_trs);
            
            E = [E_source; E_load];
            D_s = (v_ref - E(1))/(v_ref*(n_turns+1) - E(1));
            M = (E(2)/E(1))*(1-(n_turns+1)*D_s);
            i_L_mag_ref = M*i_ref*(1+1/n_turns) / ((1 - D_s)/n_turns - D_s);
            
            fds.M = M;
            fds.D_s = D_s;
            fds.i_L_mag_ref = i_L_mag_ref;
            
            fds.L = L;
            fds.L_mag = L_mag;
            fds.C = C;
            fds.E = E;
            fds.n_turns = n_turns;
            
            % v_C_ref = (1-D_s)*E(1)/(1-(n+1)*D_s);
            fds.zx_ref = [i_ref; i_L_mag_ref; v_ref];
        end
        
        function [h] = switching_function(fds, ~, zx) % t, zx
            h = fds.J_H*(zx - fds.zx_ref);
        end
        
        function [zz] = normal_space(fds, zx)
            zz = fds.T_trs*(zx - fds.zx_ref);
        end
        
        function [zx] = physical_space(fds, zz)
            zx = fds.T_inv*zz + fds.zx_ref;
        end
        
        function [zx] = nominal_state(fds)
            zx = fds.zx_ref;
        end
    end
end

