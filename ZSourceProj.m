classdef ZSourceProj < CircuitProjection
    %ZSourceProj Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        U
        num_U
    end
    
    methods
        function [zscp] = ZSourceProj(dynamicsFunction, switchingFunction1Codim2)
            %ZSourceProj Construct an instance of this class
            %   Detailed explanation goes here
            n_surfaces = 1;
            n_main = 3;
            n_alternative = 3;
            
            zz_1_2_ref = [0; 0];
            zz_3_s = 0;
            
            zscp@CircuitProjection(dynamicsFunction, switchingFunction1Codim2, ...
                n_surfaces, n_main, n_alternative, ...
                zz_1_2_ref, zz_3_s);
            
            zscp.U = dynamicsFunction.U;
            zscp.num_U = length(zscp.U);
        end
        
        function [switchingRule_cell] = critical_function(zsc, idx_S, t_fin, zz_fin, ~) % zsc, idx_S, zx_fin, u
            switchingRule_Stack = Stack();
            idx = 1;
            while idx <= zsc.num_U
                u_post = zsc.idx2ctrl(idx);
                DtV = zsc.potential_derivative(t_fin, zz_fin, u_post);
                if DtV(idx_S) < 0
                    sr = MaxConverganceRate(idx_S, t_fin, zz_fin, u_post, DtV);
                    switchingRule_Stack = switchingRule_Stack.push(sr);
                end
                idx = idx+1;
            end
                    
            switchingRule_cell = switchingRule_Stack.toCell();
        end
        
        function [zx] = alternative_space(zsc, zz)
            h = zz(1:2);
            zx = zsc.switchingFunction.physical_space([h; zsc.zz_3_s]);
        end
        
        function [u] = idx2ctrl(zsc, idx)
            u = zsc.U(:, idx);
        end
    end
end

